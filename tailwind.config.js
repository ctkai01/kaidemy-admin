/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "node_modules/flowbite-react/lib/esm/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        "primary-blue": "#1A56DB",
        "primary-black": "#2d2f31",
        "primary-gray": "#6a6f73",
        "primary-hover-gray": "#1739531f",
        "primary-border": "#d1d7dc"
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};

