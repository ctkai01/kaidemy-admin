import * as React from 'react';
import LanguageManager from '../features/LanguagesManager/LanguageManager';
import PriceManager from '../features/PricesManager/PriceManager';

export interface ILanguagesPageProps {
}

export default function LanguagesPage (props: ILanguagesPageProps) {
  return (
    <div>
        <LanguageManager/>
    </div>
  );
}
