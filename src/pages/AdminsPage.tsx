import AdminManager from '../features/AdminsManager/AdminManager';

export interface IAdminsPageProps {
}

export default function AdminsPage (props: IAdminsPageProps) {
  return (
    <div>
      <AdminManager/>
    </div>
  );
}
