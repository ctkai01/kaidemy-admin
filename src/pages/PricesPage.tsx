import * as React from 'react';
import PriceManager from '../features/PricesManager/PriceManager';

export interface IPricesPageProps {
}

export default function PricesPage (props: IPricesPageProps) {
  return (
    <div>
        <PriceManager/>
    </div>
  );
}
