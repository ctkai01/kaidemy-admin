import CategoryManager from '../features/CategoryManager/CategoryManager';

export interface ICategoriesPageProps {
}

export default function CategoriesPage (props: ICategoriesPageProps) {
  return (
    <div>
        <CategoryManager/>
    </div>
  );
}
