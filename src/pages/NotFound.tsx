import { Button } from "flowbite-react";
import { Link } from "react-router-dom";
import notFoundGif from "../assets/images/404.gif";

const NotFound = () => {
  return (
    <div className="bg-primary-black h-screen">
      <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
        <div className="flex flex-col items-center">
          <div className="max-w-lg"> 

          <img src={"https://flowbite-admin-dashboard.vercel.app/images/illustrations/404.svg"} loading="lazy" />
          </div>
          
          <div className="font-bold text-[48px] text-white">
            Page not found
          </div>

          <Button
            className="mt-5 w-fit font-bold"
            // outline
            // gradientDuoTone="cyanToBlue"
          >
            <Link to="/">Go Home</Link>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
