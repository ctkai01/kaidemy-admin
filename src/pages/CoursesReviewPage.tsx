import CourseReviewsManage from '../features/CourseReviewsManage/CourseReviewsManage';

export interface ICoursesReviewPageProps {
}

export default function CoursesReviewPage (props: ICoursesReviewPageProps) {
  return (
    <div>
        <CourseReviewsManage/>
    </div>
  );
}
