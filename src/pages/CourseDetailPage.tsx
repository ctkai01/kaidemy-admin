import {
    Breadcrumb,
    CustomFlowbiteTheme,
    Flowbite,
    Tabs,
    TabsRef,
} from "flowbite-react";
import * as React from "react";
import { HiHome } from "react-icons/hi";
import { IoIosArrowForward } from "react-icons/io";
import { RiFolderVideoFill } from "react-icons/ri";
import {
    Link,
    useNavigate,
    useParams,
    useSearchParams,
} from "react-router-dom";
import { COURSE_DETAIL_TABS } from "../constants";
import CourseDetail from "../features/CourseManager/CourseDetail/CourseDetail";
import CourseIntroduce from "../features/CourseManager/CourseIntroduce";
import CourseNotFound from "../features/CourseManager/CourseNotFound";
import { Course, CourseDetailRes } from "../models/course";
import { CourseServiceApi } from "../services/api/courseServiceApi";

export interface ICourseDetailPageProps {}

const customTabtheme: CustomFlowbiteTheme = {
    tabs: {
        tablist: {
            base: "grid !gap-4 grid-flow-col auto-cols-max w-full ml-auto mr-auto   px-6 ",
            tabitem: {
                base: "!text-primary-gray  font-bold px-1 py-2 flex justify-center items-center !border-b-4",
                styles: {
                    underline: {
                        active: {
                            on: "border-b-cyan-600 !text-primary-black",
                            off: "hover:border-b-cyan-600",
                        },
                    },
                },
            },
        },
        tabitemcontainer: {
            base: "bg-white pt-4",
        },
        tabpanel: "ml-auto mr-auto  px-6 pt-4 pb-12",
    },
};

export default function CourseDetailPage(props: ICourseDetailPageProps) {
    const [course, setCourse] = React.useState<CourseDetailRes | null>(null);
    const tabsRef = React.useRef<TabsRef>(null);
    const [activeTab, setActiveTab] = React.useState(0);
    const navigate = useNavigate();

    const [searchParams, setSearchParams] = useSearchParams();

    const { id: courseID } = useParams();
    React.useEffect(() => {
        const fetchCourse = async () => {

            try {
                if (courseID && !isNaN(+courseID)) {
                    const courseResult = await CourseServiceApi.getCurriCulumsByCourseByID(+courseID)
                    setCourse(courseResult);
                } else {
                    console.log("Is not course ID");
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourse();
    }, []);

    React.useLayoutEffect(() => {
        const typeInfo = searchParams.get("type");
        if (typeInfo) {
            if (typeInfo === "detail") {
                tabsRef.current?.setActiveTab(COURSE_DETAIL_TABS.DETAIL);
                setActiveTab(COURSE_DETAIL_TABS.DETAIL);
            } else {
                tabsRef.current?.setActiveTab(COURSE_DETAIL_TABS.INTRODUCE);
                setActiveTab(COURSE_DETAIL_TABS.INTRODUCE);

            }
        } else {
            tabsRef.current?.setActiveTab(COURSE_DETAIL_TABS.INTRODUCE);
            setActiveTab(COURSE_DETAIL_TABS.INTRODUCE);
        }
    }, []);

    const handleChangeTab = (tab: number) => {
        if (tab === COURSE_DETAIL_TABS.INTRODUCE) {
            navigate("?type=introduce");
        } else {
            navigate("?type=detail");
        }
    };
    return (
        <div>
            <div className="ml-2 flex items-center text-sm font-bold text-primary-black ">
                <Link to="/courses" className="flex items-center">
                    <RiFolderVideoFill className="mr-2 h-4 w-4" />
                    Khóa học
                </Link>
                <IoIosArrowForward className="mx-2" />

                <Link className="text-primary-gray" to="">
                    Chi tiết khóa học
                </Link>
            </div>

            {course ? (
                <Flowbite theme={{ theme: customTabtheme }}>
                    <Tabs
                        aria-label="Tabs with icons"
                        style="underline"
                        ref={tabsRef}
                        onActiveTabChange={handleChangeTab}
                    >
                        <Tabs.Item
                            active={activeTab === COURSE_DETAIL_TABS.INTRODUCE}
                            title="Giới thiệu"
                        >
                            <CourseIntroduce course={course} />
                        </Tabs.Item>
                        <Tabs.Item
                            active={activeTab === COURSE_DETAIL_TABS.DETAIL}
                            title="Chi tiết"
                        >
                            <CourseDetail course={course} />
                        </Tabs.Item>
                    </Tabs>
                </Flowbite>
            ) : (
                <CourseNotFound />
            )}
        </div>
    );
}
