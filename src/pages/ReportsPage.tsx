import CourseReviewsManage from '../features/CourseReviewsManage/CourseReviewsManage';
import ReportsManage from '../features/ReportsManage/ReportsManageManage';

export interface IReportsPagePageProps {
}

export default function ReportsPagePage (props: IReportsPagePageProps) {
  return (
      <div>
          <ReportsManage />
      </div>
  );
}
