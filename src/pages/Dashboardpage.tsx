import * as React from 'react';
import DashboardManager from '../features/Dashboard/DashboardManager';

export interface IDashboardPageProps {
}

export default function DashboardPage (props: IDashboardPageProps) {
  return (
    <div>
      <DashboardManager/>
    </div>
  );
}
