import { Card } from "flowbite-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { ADMIN_NOR_ROLE, ADMIN_SUPPER_ROLE } from "../constants";
import LoginForm from "../features/Auth/Login/LoginForm";
import { useAppDispatch } from "../hook/redux-hook";
import { ResponseLogin, SignIn } from "../models";
import { UserServiceApi } from "../services/api/userServiceApi";
import { login } from "../services/state/redux/authSlide";
import { setAuthLocalStorage } from "../utils";

export default function Login() {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const handleLogin = async (data: SignIn) => {
        try {
            const loginResult = await UserServiceApi.login(data);
            if (
                loginResult.user.role != ADMIN_SUPPER_ROLE &&
                loginResult.user.role != ADMIN_NOR_ROLE
            ) {
                toast(
                    <div className="font-bold">
                        Need login with admin account
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "error",
                        theme: "colored",
                    },
                );
            } else {
                setAuthLocalStorage(loginResult.token);
                const payloadLogin: ResponseLogin = {
                    user: loginResult.user,
                };
                dispatch(login(payloadLogin));
                navigate("/");
            }
        } catch (e: any) {
            console.log(e);
            if (e.response.status === 401) {
                toast(
                    <div className="font-bold">
                        Email or password incorrect
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "error",
                        theme: "colored",
                    },
                );
            }
            console.log(e);
        }
    };
    return (
        <div className="flex h-screen items-center justify-center bg-primary-black">
            <Card className="w-[400px] max-w-md">
                <div className="flex justify-center">
                    <img
                        className="w-[100px]"
                        src="https://kaidemy.b-cdn.net/avatar/hustdemy-high-logo-transparent-black.png"
                        loading="lazy"
                    />
                </div>
                <LoginForm handleLogin={handleLogin} />
            </Card>
        </div>
    );
}
