import { Button } from 'flowbite-react';
import * as React from 'react';
import { Link } from 'react-router-dom';

export default function Forbbiden () {
  return (
    <div  className='bg-orange-500 h-screen flex justify-center items-center'>
        <div>
            <div className='text-[120px] text-center font-bold text-white'>403</div>
            <div className='text-[48px] text-white text-center'>This is fobbiden area</div>
            <div className='flex justify-center mt-6'>
                <Link to={"/"}>
                    <Button>
                    Go Home
                    </Button>
                </Link> 
            </div>
                
        </div>
    </div>
  );
}
