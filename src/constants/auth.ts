export const ADMIN_SUPPER_ROLE = 0
export const ADMIN_NOR_ROLE = 1
export const NORMAL_ROLE = 2;
export const TEACHER_ROLE = 3;

export const TEACHER_FILTER = 0;
export const NORMAL_FILTER = 1;
export const BLOCK_FILTER = 2;
export const NOT_BLOCK_FILTER = 3;
export const ALL = 4;