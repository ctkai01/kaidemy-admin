export const REVIEW_INIT_STATUS = 0
export const REVIEW_PENDING_STATUS = 1
export const REVIEW_VERIFY_STATUS = 2


export const COURSE_DETAIL_TABS = {
    INTRODUCE: 0,
    DETAIL: 1,
};

export const LECTURE_RESOURCE_ASSET_TYPE = 1;
export const LECTURE_WATCH_ASSET_TYPE = 2; 

export const LECTURE_TYPE = 1;
export const QUIZ_TYPE = 2; 