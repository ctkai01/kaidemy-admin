import { axiosClientService } from "./axiosClientService";

export const CourseServiceApi = {
    getCourseReviews: (size: number, page: number): Promise<any> => {
        const url = `/courses/reviews?size=${size}&page=${page}&order=DESC`;
        return axiosClientService.get(url);
    },

    approvalCourseReviews: (id: number, data: any): Promise<any> => {
        const url = `/courses/${id}/approval-review`;
        return axiosClientService.post(url, data);
    },

    getCurriCulumsByCourseByID: (id: number): Promise<any> => {
        const url = `/courses/${id}/curriculums`;
        return axiosClientService.get(url);
    },

    getCourse: (size: number, page: number): Promise<any> => {
        const url = `/courses?size=${size}&page=${page}&filterOrder=1`;
        return axiosClientService.get(url);
    },

    getReports: (size: number, page: number): Promise<any> => {
        const url = `/reports?size=${size}&page=${page}&&order=DESC`;
        return axiosClientService.get(url);
    },

    getOverviewAdmins: (): Promise<any> => {
        const url = `/courses/overview/admin`;
        return axiosClientService.get(url);
    },

};
