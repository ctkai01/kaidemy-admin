import { axiosClientService } from "./axiosClientService";

export const CategoryServiceApi = {
    getPrices: (size: number, page: number): Promise<any> => {
        const url = `/prices?size=${size}&page=${page}`;
        return axiosClientService.get(url);
    },

    createPrice: (data: any): Promise<any> => {
        const url = `/prices`;
        return axiosClientService.post(url, data);
    },

    updatePrice: (data: any, id: number): Promise<any> => {
        const url = `/prices/${id}`;
        return axiosClientService.put(url, data);
    },

    deletePrice: (id: number): Promise<any> => {
        const url = `/prices/${id}`;
        return axiosClientService.delete(url);
    },

    createLevel: (data: any): Promise<any> => {
        const url = `/levels`;
        return axiosClientService.post(url, data);
    },

    updateLevel: (data: any, id: number): Promise<any> => {
        const url = `/levels/${id}`;
        return axiosClientService.put(url, data);
    },

    deleteLevel: (id: number): Promise<any> => {
        const url = `/levels/${id}`;
        return axiosClientService.delete(url);
    },

    getLevels: (size: number, page: number): Promise<any> => {
        const url = `/levels?size=${size}&page=${page}&order=DESC`;
        return axiosClientService.get(url);
    },

    //Language
    createLanguage: (data: any): Promise<any> => {
        const url = `/languages`;
        return axiosClientService.post(url, data);
    },

    updateLanguage: (data: any, id: number): Promise<any> => {
        const url = `/languages/${id}`;
        return axiosClientService.put(url, data);
    },

    deleteLanguage: (id: number): Promise<any> => {
        const url = `/languages/${id}`;
        return axiosClientService.delete(url);
    },

    getLanguages: (size: number, page: number): Promise<any> => {
        const url = `/languages?size=${size}&page=${page}&order=DESC`;
        return axiosClientService.get(url);
    },

    //Issue Types
    createIssueType: (data: any): Promise<any> => {
        const url = `issue-types`;
        return axiosClientService.post(url, data);
    },

    updateIssueType: (data: any, id: number): Promise<any> => {
        const url = `issue-types/${id}`;
        return axiosClientService.put(url, data);
    },

    deleteIssueType: (id: number): Promise<any> => {
        const url = `issue-types/${id}`;
        return axiosClientService.delete(url);
    },

    getIssueTypes: (size: number, page: number): Promise<any> => {
        const url = `issue-types?size=${size}&page=${page}`;
        return axiosClientService.get(url);
    },

    //Categories
    createCategory: (data: any): Promise<any> => {
        const url = `categories`;
        return axiosClientService.post(url, data);
    },

    updateCategory: (data: any, id: number): Promise<any> => {
        const url = `categories/${id}`;
        return axiosClientService.put(url, data);
    },

    deleteCategory: (id: number): Promise<any> => {
        const url = `categories/${id}`;
        return axiosClientService.delete(url);
    },

    getCategories: (
        size: number,
        page: number,
        search?: string,
    ): Promise<any> => {
        let url = `categories?size=${size}&page=${page}&order=DESC`;

        if (search || search === "0") {
            url += `&search=${search}`;
        }
        return axiosClientService.get(url);
    },

    getParentCategories: (): Promise<any> => {
        const url = `categories?parentID=-1&size=${1000}&page=${1}&order=DESC`;

        return axiosClientService.get(url);
    },

    getCategoriesFilter: (
        size: number,
        page: number,
        id: number,
        search?: string,
    ): Promise<any> => {
        let url = `categories?parentID=${id}&size=${size}&page=${page}`;
        if (search || search === "0") {
            url += `&search=${search}`;
        }
        return axiosClientService.get(url);
    },
};
