import {
    NORMAL_FILTER,
    TEACHER_FILTER,
    TEACHER_ROLE,
    NORMAL_ROLE,
    BLOCK_FILTER,
    NOT_BLOCK_FILTER,
} from "../../constants/auth";
import { ALL } from "../../constants";
import { SignIn } from "../../models/auth";
import { axiosClientService } from "./axiosClientService";

export const UserServiceApi = {
    login: (data: SignIn): Promise<any> => {
        const url = "/auth/login";
        return axiosClientService.post(url, data);
    },

    getNormalAdmins: (size: number, page: number): Promise<any> => {
        const url = `/admins?size=${size}&page=${page}`;
        return axiosClientService.get(url);
    },

    createNormalAdmin: (data: any): Promise<any> => {
        const url = `/admins`;
        return axiosClientService.post(url, data);
    },

    updateNormalAdmin: (data: any, id: number): Promise<any> => {
        const url = `/admins/${id}`;
        return axiosClientService.put(url, data);
    },

    deleteNormalAdmin: (id: number): Promise<any> => {
        const url = `/admins/${id}`;
        return axiosClientService.delete(url);
    },

    getUsers: (
        size: number,
        page: number,
        search: string | null,
        filter: number | null,
    ): Promise<any> => {
        let roles = [NORMAL_ROLE, TEACHER_ROLE];
        let url = `/users?size=${size}&page=${page}`;
        if (search) {
            url += `&search=${search}`;
        }

        if (filter === ALL) {
            url += ``;
        }

        if (filter === NORMAL_FILTER) {
           
            roles = [NORMAL_ROLE];
        }

        if (filter === TEACHER_FILTER) {
            roles = [TEACHER_ROLE];
        }

        if (filter === BLOCK_FILTER) {
            url += `&isBlock=1`;

        }

        if (filter === NOT_BLOCK_FILTER) {
            url += `&isBlock=0`;

        }

        url += `&roles=${roles.join(",")}`;

        return axiosClientService.get(url);
    },

    blockUser: (data: any, id: number): Promise<any> => {
        const url = `/users/${id}/block`;
        return axiosClientService.put(url, data);
    },
    
};
