import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ResponseLogin, User } from "../../../models/auth";
import { RootState } from "./store";

export interface AuthState {
    isLoggedIn: boolean;
    currentUser?: User;
}

const initialState: AuthState = {
    isLoggedIn: false,
    currentUser: undefined,
};

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        login(state, action: PayloadAction<ResponseLogin>) {
            state.currentUser = action.payload.user;
            state.isLoggedIn = true;
        },
       
        logout(state) {
            state.currentUser = undefined;
            state.isLoggedIn = false;
        },
    },
});

export const { login, logout } = authSlice.actions;

export const selectIsLoggedIn = (state: RootState): boolean => state.auth.isLoggedIn;
export const selectUserAuth = (state: RootState): User | undefined =>
    state.auth.currentUser;


export default authSlice.reducer;
