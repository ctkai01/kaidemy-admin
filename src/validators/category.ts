import * as Yup from "yup";
import { CreateCategory, CreateIssueType, CreateLanguage, CreateLevel, CreatePrice, UpdateCategory, UpdateIssueType, UpdateLanguage, UpdateLevel, UpdatePrice } from "../models";

export const schemeCreatePrice: Yup.ObjectSchema<CreatePrice> = Yup.object({
    tier: Yup.string()
        .required("Loại là bắt buộc")
        .max(60, "Loại lớn hơn 60 ký tự"),
    value: Yup.number()
        .typeError("Giá trị phải là số hợp lệ")
        .required("Giá trị là bắt buộc")
        .test("is-positive", "Giá trị phải là số dương", (value) => {
            // Add your custom validation logic here
            return value === undefined || value === null || value > 0;
        }),
});


export const schemeUpdatePrice: Yup.ObjectSchema<UpdatePrice> = Yup.object({
    id: Yup.number().required(),
    tier: Yup.string()
        .required("Loại là bắt buộc")
        .max(60, "Loại lớn hơn 60 ký tự"),
    value: Yup.number()
        .typeError("Giá trị phải là số hợp lệ")
        .required("Giá trị là bắt buộc")
        .test("is-positive", "Giá trị phải là số dương", (value) => {
            // Add your custom validation logic here
            return value === undefined || value === null || value > 0;
        }),
});

export const schemeCreateLevel: Yup.ObjectSchema<CreateLevel> = Yup.object({
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
});

export const schemeUpdateLevel: Yup.ObjectSchema<UpdateLevel> = Yup.object({
    id: Yup.number().required(),
    name: Yup.string()
        .required("Tên là bắt buộc")
         .max(60, "Tên lớn hơn 60 ký tự"),
});

export const schemeCreateLanguage: Yup.ObjectSchema<CreateLanguage> = Yup.object({
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
});

export const schemeUpdateLanguage: Yup.ObjectSchema<UpdateLanguage> = Yup.object({
    id: Yup.number().required(),
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
});

export const schemeCreateIssueType: Yup.ObjectSchema<CreateIssueType> = Yup.object({
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
});

export const schemeUpdateIssueType: Yup.ObjectSchema<UpdateIssueType> = Yup.object({
    id: Yup.number().required(),
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
});


export const schemeCreateCategory: Yup.ObjectSchema<CreateCategory> = Yup.object({
    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
    parent_id: Yup.number().required("Danh mục cha là bắt buộc"),
});

export const schemeUpdateCategory: Yup.ObjectSchema<UpdateCategory> = Yup.object({
    id: Yup.number().required(),

    name: Yup.string()
        .required("Tên là bắt buộc")
        .max(60, "Tên lớn hơn 60 ký tự"),
    parent_id: Yup.number().required("Danh mục cha là bắt buộc"),
});