import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ADMIN_SUPPER_ROLE } from "./constants";
import { useAppSelector } from "./hook/redux-hook";
import Layout from "./layouts/Layout";
import AdminsPage from "./pages/AdminsPage";
import CategoriesPage from "./pages/CategoriesPage";
import CourseDetailPage from "./pages/CourseDetailPage";
import CoursesPage from "./pages/CoursesPage";
import CoursesReviewPage from "./pages/CoursesReviewPage";
import DashboardPage from "./pages/Dashboardpage";
import Forbbiden from "./pages/Forbbiden";
import IssueTypesPage from "./pages/IssueTypePage";
import LanguagesPage from "./pages/LanguagesPage";
import LevelsPage from "./pages/LevelsPage";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import PricesPage from "./pages/PricesPage";
import ReportsPagePage from "./pages/ReportsPage";
import UsersPage from "./pages/UsersPage";
import { selectIsLoggedIn, selectUserAuth } from "./services/state/redux/authSlide";
import config from "./configs/configs";

function App() {
  const isLoggedIn = useAppSelector(selectIsLoggedIn);
  const user = useAppSelector(selectUserAuth);
  const isSupperAdmin = user?.role === ADMIN_SUPPER_ROLE

    console.log("API URL:", config.apiUrl);
  return (
      <>
          <ToastContainer />
          <BrowserRouter>
              <Routes>
                  <Route
                      path="/login"
                      element={isLoggedIn ? <Navigate to="/" /> : <Login />}
                  />
                  <Route path="/" Component={Layout}>
                      <Route
                          index
                          element={
                              isLoggedIn ? (
                                  <DashboardPage />
                              ) : (
                                  <Navigate to="/login" />
                              )
                          }
                      />
                      <Route path="users" element={<UsersPage />} />
                      <Route path="courses" element={<CoursesPage />} />
                      <Route path="prices" element={<PricesPage />} />
                      <Route path="levels" element={<LevelsPage />} />
                      <Route path="languages" element={<LanguagesPage />} />
                      <Route
                          path="approve-courses"
                          element={<CoursesReviewPage />}
                      />
                      <Route path="reports" element={<ReportsPagePage />} />
                      <Route path="categories" element={<CategoriesPage />} />
                      <Route
                          path="courses/:id"
                          element={<CourseDetailPage />}
                      />
                      {/* <Route
                    path="course/:id/detail"
                    element={<CategoriesPage/>}  
                />    */}

                      <Route
                          path="categories/:id/sub-categories"
                          element={<CategoriesPage />}
                      />
                      <Route path="issue-types" element={<IssueTypesPage />} />
                      <Route
                          path="admins"
                          element={
                              isSupperAdmin ? <AdminsPage /> : <Forbbiden />
                          }
                      />
                  </Route>
                  {/* <Route path="*" element={<Forbbiden />} /> */}
                  <Route path="*" element={<NotFound />} />
              </Routes>
          </BrowserRouter>
      </>
  );
}

export default App;
