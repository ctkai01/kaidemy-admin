import { Outlet } from "react-router-dom";
import Header from "../components/ui/core/Header";
import Sidebar from "../components/ui/core/Sidebar";
// import Footer from "../components/ui/core/Footer";
// import Header from "../components/ui/core/Header/Header";


function Layout() {
    return (
        <>
            <Header />
            
            <div className="flex">
                <Sidebar/>
                <div className="flex-1 pt-[30px]">
                    <Outlet />
                </div>
            </div>
        </>
    );
}

export default Layout;
