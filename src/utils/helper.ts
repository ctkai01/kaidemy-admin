import {
    LECTURE_WATCH_ASSET_TYPE,
    LECTURE_RESOURCE_ASSET_TYPE,
    QUIZ_TYPE,
    LECTURE_TYPE,
} from "../constants";
import { ContentArticle, ContentCourse, ContentQuiz, ContentVideo, NavigateLecture } from "../features/CourseManager/CourseDetail/CourseDetail";
import { Asset, Curriculum, Lecture } from "../models/course";

export function setAuthLocalStorage(token: string) {
  localStorage.setItem("token", token);
}

export function getAuthLocalStorage(): string | null {
  return localStorage.getItem("token");
}

export function clearAuthLocalStorage() {
  localStorage.removeItem("token");
}

export function formatBetweenNumberPaginate(currentPage: number, totalPerPage:number, total: number): string {
    const remainItem  = total - (currentPage - 1) * totalPerPage
    const toIndex =
        remainItem >= totalPerPage
            ? currentPage * totalPerPage
            : (currentPage - 1) * totalPerPage + remainItem;

    return `${(currentPage - 1) * totalPerPage + 1}-${toIndex}`;
    }
const MINUTE = 60
export const totalTimeLecture = (lectures: Lecture[]): string => {
  const totalSeconds = lectures.reduce((total, lecture, _) => {
    const assetWatch: Asset | undefined = lecture.assets.find(
        (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
    ); 
    if (assetWatch) {
      return total + assetWatch.duration; 
    }

    if (lecture.type === LECTURE_TYPE && lecture.article) {
      return total + MINUTE; 

    }
    return total;
  }, 0)
  return convertSecondsToHoursMinutes(totalSeconds);
}

export const totalTimeAsset = (assets: Asset[]): number => {
    const totalSeconds = assets.reduce((total, asset, _) => {
        if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
          return total + asset.duration
        } else {
          return total;
        }
    }, 0);
    return totalSeconds;
};

export function convertSecondsToHoursMinutes(totalSeconds: number): string {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    // const seconds = totalSeconds % 60;

    return `${hours}h ${minutes}m`;
}

export function getResources(assets: Asset[]): Asset[] {
  const resources = assets.filter(
      (asset) => asset.type === LECTURE_RESOURCE_ASSET_TYPE,
  );

  return resources;
}

export function generateTextEllipsis(text: string): string {
  if (text.length > 50) {
    return text.slice(0,50) + "…"
  } else {
    return text
  }
}

export function generatePrefixNavigateLecture(lectureNavigate: NavigateLecture): string {
  if (lectureNavigate.lecture.type === QUIZ_TYPE) {
    return `Quiz ${lectureNavigate.indexNumber}: ${lectureNavigate.lecture.title}`;
  } else {
    return `${lectureNavigate.indexNumber}. ${lectureNavigate.lecture.title}`;

  }
}
export function initContentCourse(curriculums: Curriculum[]): ContentCourse | null {
    if (curriculums.length) {
        if (curriculums[0].lectures.length) {
            if (curriculums[0].lectures[0].type === QUIZ_TYPE) {
              const contentQuiz: ContentQuiz = {
                  // article: curriculums[0].lectures[0].article,
                  indexNumber: "Quiz 1:",
                  title: curriculums[0].lectures[0].title,
                  questions: curriculums[0].lectures[0].questions
              }; 
              return {
                  id: curriculums[0].lectures[0].id,
                  type: "quiz",
                  content: contentQuiz,
                  index: 0,
                  curriculumID: curriculums[0].id
              };
            } else {
                if (curriculums[0].lectures[0].article) {
                    const contentArticle: ContentArticle = {
                        article: curriculums[0].lectures[0].article,
                        title: curriculums[0].lectures[0].title
                    };
                    return {
                        id: curriculums[0].lectures[0].id,
                        type: "article",
                        content: contentArticle,
                        index: 0,
                        curriculumID: curriculums[0].id,
                    };
                }

                const assetWatch = curriculums[0].lectures[0].assets.find(
                    (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
                );

                if (assetWatch) {
                  const contentVideo: ContentVideo = {
                      url: assetWatch.url,
                  };
                   return {
                       id: curriculums[0].lectures[0].id,
                       type: "video",
                       content: contentVideo,
                       index: 0,
                       curriculumID: curriculums[0].id,
                   };
                } else {
                  return null
                }
            }
            // const assetWatch = curriculums[0].lectures[0].assets.find(
            //     (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
            // );
        } else {
          return null
        }
    } else {
      return null
    }

    // course.curriculums ?  course.curriculums[0].lectures ? course.curriculums[0].lectures[0].assets   : []
} 

export function akaName(name: string): string {
    const splitName = name.split(" ");
    if (splitName.length === 1) {
        return splitName[0][0].toUpperCase();
    } else {
        return (
            splitName[0][0].toUpperCase() +
            splitName[splitName.length - 1][0].toUpperCase()
        );
    }
}