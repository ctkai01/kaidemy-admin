import { Category, Language, Level, Price } from "./category";

export interface Course {
    id: number;
    outComes: string[] | null;
    intendedFor: string[] | null;
    requirements: string[] | null;
    productIdStripe: string;
    level: Level;
    category: Category;
    subCategory: number;
    title: string;
    reviewStatus: number;
    welcomeMessage: string | null;
    congratulationsMessage: string | null;
    subtitle: string | null;
    primarilyTeach: string | null;
    description: string | null;
    language: Language;
    price: Price;
    user: {
        id: number;
        name: string;
    };
    promotionalVideo: string | null;
    image: string | null;
    curriculums: Curriculum[];
    updated_at: string;
    created_at: string;
}

export interface CourseDetailRes {
    id: number;
    outComes: string[] | null;
    intendedFor: string[] | null;
    requirements: string[] | null;
    productIdStripe: string;
    level: LevelRes | null;
    category: CategoryRes;
    subCategory: CategoryRes;
    title: string;
    reviewStatus: number;
    welcomeMessage: string | null;
    congratulationsMessage: string | null;
    subtitle: string | null;
    primarilyTeach: string | null;
    description: string | null;
    status: number;
    language: LanguageRes | null;
    price: PriceRes | null;
    user: UserRes;
    promotionalVideo: string | null;
    image: string | null;
    curriculums: Curriculum[];
    updated_at: string;
    created_at: string;
}

interface LevelRes {
    id: number;
    name: string;
}

interface CategoryRes {
    id: number;
    name: string;
    parent_id: number | null;
}

interface LanguageRes {
    id: number;
    name: string;
}

interface PriceRes {
    id: number;
    tier: string;
    value: number;
}

interface UserRes {
    id: number;
    name: string;
}

export interface Curriculum {
    id: number;
    title: string;
    description: string | null;
    courseID: number;
    lectures: Lecture[];
    updated_at: string;
    created_at: string;
}

export interface Lecture {
    id: number;
    title: string;
    type: number;
    article: string | null;
    curriculumID: number;
    description: string | null;
    isPromotional: boolean;
    assets: Asset[];
    questions: Question[];
    order: number;
    updated_at: string;
    created_at: string;
}

export interface Asset {
    id: number;
    bunnyID: string;
    url: string;
    type: number;
    duration: number;
    name?: string;
    size: number;
    lectureID: number;
    updated_at: string;
    created_at: string;
}

export interface Question {
    id: number;
    lectureID: number;
    title: string;
    answers: Answer[];
    updated_at: string;
    created_at: string;
}

export interface Answer {
    id: number;
    answerText: string;
    explain: string | null;
    isCorrect: boolean;
    questionID: number;
    updatedAt: string;
    createdAt: string;
}

export interface ChoseAnswer {
    answer1: string;
    answer2: string;
    answer3: string;
    answer4: string;
}

export interface Report {
    id: number;
    user: UserReport;
    course: CourseReport;
    issueType: IssueTypeReport;
    description: string;
    updated_at: string;
    created_at: string;
}

interface UserReport {
    id: number;
    name: string;
    avatar: string;
}

interface CourseReport {
    id: number;
    title: string;
    image: string | null;
}

interface IssueTypeReport {
    id: number;
    name: string;
}

export interface OverviewAdmin {
    courses: {
        total: number;
        totalThisMonth: number;
        detailStats: number[];
    };
    users: {
        total: number;
        totalThisMonth: number;
        detailStats: number[];
    };
    revenue: {
        platform: Revenue;
        fee: Revenue;
    };
}

interface Revenue {
    total: number;
    totalThisMonth: number;
    detailStats: number[];
}
