export interface SignUp {
  name: string;
  email: string;
  password: string;
}

export interface SignIn {
  email: string;
  password: string;
}

export interface ResponseLogin {
//   token: string;
  user: User;
}

export interface User {
  id: number;
  email: string;
  name: string;
  typeAccount: number;
  avatar: string;
  headline: string;
  biography: string;
  websiteURL: string;
  twitterURL: string;
  facebookURL: string;
  linkedinURL: string;
  youtubeURL: string;
  role: number;
  isBlock: boolean;
  updated_at: string;
  created_at: string;
}

export interface CreateAdmin {
    name: string;
    email: string;
    password: string;
}


export interface UpdateAdmin {
    id: number;
    name?: string;
    password?: string;

}