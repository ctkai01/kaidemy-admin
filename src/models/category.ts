export interface Language {
    id: number;
    name: string
    created_at: string;
    updated_at: string
}

export interface IssueType {
    id: number;
    name: string
    created_at: string;
    updated_at: string
}

export interface Category {
    id: number;
    name: string;
    parentID: number | null;
    created_at: string;
    updated_at: string;
}

export interface Price {
    id: number;
    tier: string;
    value: number;
    created_at: string;
    updated_at: string;
}

export interface PaginationInfo {
    totalPage: number
    totalItem: number
    size: number
}
// children: null;
// created_at: "2023-10-08T16:20:43.9+07:00";
// id: 2;
// name: "WebPython";
// parent: null;
// parent_id: null;
// updated_at: "2023-10-08T16:20:43.9+07:00";
export interface Level {
    id: number;
    name: string
    created_at: string;
    updated_at: string
}

export interface CreatePrice {
    tier: string;
    value: number;
}

export interface UpdatePrice {
    id: number;
    tier: string;
    value: number;
}

export interface CreateLevel {
    name: string;
}


export interface UpdateLevel {
    id: number;
    name: string;
}

export interface CreateLanguage {
    name: string;
}


export interface UpdateLanguage {
    id: number;
    name: string;
}

export interface CreateIssueType {
    name: string;
}

export interface UpdateIssueType {
    id: number;
    name: string;
}

export interface CreateCategory {
    name: string;
    parent_id: number
}

export interface UpdateCategory {
    id: number;
    name: string;
    parent_id: number
}