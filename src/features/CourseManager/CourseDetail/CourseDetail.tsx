import { useMemo, useState } from "react";
import { QUIZ_TYPE } from "../../../constants";
import { Course, CourseDetailRes, Lecture, Question } from "../../../models/course";
import { initContentCourse } from "../../../utils";
import SectionLectureSidebar from "./SectionLectureSibar";
import VideoLearning from "./VideoLearning";

export interface ICourseDetailProps {
    course: CourseDetailRes;
}

export interface ContentCourse {
    type: "quiz" | "video" | "article" | "undefine";
    id: number;
    curriculumID: number;
    index: number
    content: ContentQuiz | ContentVideo | ContentArticle | null;
}

export interface ContentQuiz {
    questions: Question[]
    title: string
    indexNumber: string
}

export interface ContentVideo {
    url: string;
}

export interface ContentArticle {
    article: string;
    title: string;
}

export interface NavigateLecture {
    lecture: Lecture;
    indexNumber: number;
    curriculumID: number;
}

export default function CourseDetail(props: ICourseDetailProps) {
    const { course } = props;
    // course.curriculums ?  course.curriculums[0].lectures ? course.curriculums[0].lectures[0].assets   : []
    const [contentCourse, setContentCourse] = useState<ContentCourse | null>(
        initContentCourse(course.curriculums ? course.curriculums : []),
    );
    const [isFinishCourse, setIsFinishCourse] = useState(false)
    const handleContentCourse = (content: ContentCourse | null) => {
        setContentCourse(content);
    };

    const handleFinishCourse = () => {
        setIsFinishCourse(true)
    }

     const handleResetFinishCourse = () => {
         setIsFinishCourse(false);
     };

    const lectureList = useMemo(() => {
        const result: NavigateLecture[] = [];
        const indexLecture = {
            quiz: 1,
            article: 1,
        };
        course.curriculums.forEach(curriculum => {
            curriculum.lectures.forEach(lecture => {
                const indexNumber =
                    lecture.type === QUIZ_TYPE
                        ? indexLecture.quiz++
                        : indexLecture.article++;
                result.push({
                    lecture: lecture,
                    indexNumber: indexNumber,
                    curriculumID: curriculum.id
                });
            })
        })
        return result;
    }, [])

    return (
        <div>
            <div className="flex">
                <div style={{ flex: "3" }}>
                    <VideoLearning
                        lectureList={lectureList}
                        contentCourse={contentCourse}
                        isFinishCourse={isFinishCourse}
                        handleFinishCourse={handleFinishCourse}
                        handleContentCourse={handleContentCourse}
                        handleResetFinishCourse={handleResetFinishCourse}
                    />
                </div>
                <div className="" style={{ flex: "1" }}>
                    <SectionLectureSidebar
                        course={course}
                        contentCourse={contentCourse}
                        handleContentCourse={handleContentCourse}
                        handleResetFinishCourse={handleResetFinishCourse}
                        // handleSetVideoURL={handleSetVideoURL}
                    />
                </div>
            </div>
        </div>
    );
}
