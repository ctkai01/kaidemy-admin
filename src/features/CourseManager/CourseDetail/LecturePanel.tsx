import { totalTimeLecture } from "../../../utils";

export interface ILecturePanelProps {
    title: string;
    numberLecture: number;
    time: string;
    index: number;
}

export default function LecturePanel (props: ILecturePanelProps) {
  const { numberLecture, index, time, title } = props;
  return (
      <div>
          <div>
              <span className="line-clamp-2 flex-1 text-base font-bold text-primary-black">
                  Phần {index + 1}: {title}
              </span>
          </div>

          <span className="mt-1 text-xs text-primary-black">
             Tổng cộng {numberLecture} | {time}
              {/* 1 / {numberLecture} | {time} */}
          </span>
      </div>
  );
}
