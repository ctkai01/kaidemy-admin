import * as React from "react";
import { IoClose } from "react-icons/io5";
import { LECTURE_TYPE, QUIZ_TYPE } from "../../../constants";
import { Course, CourseDetailRes } from "../../../models/course";
import { totalTimeLecture } from "../../../utils";
import AccordionController from "./AccordionController";
import AccordionItem from "./AccordionItem";
import { ContentCourse } from "./CourseDetail";
import LearningLectureItem from "./LearningLectureItem";
import LecturePanel from "./LecturePanel";

export interface ISectionLectureSidebarProps {
    course: CourseDetailRes;
    contentCourse: ContentCourse | null;
    handleContentCourse: (content: ContentCourse | null) => void;
    handleResetFinishCourse: () => void;
    // handleSetVideoURL: (url: string) => void
}

export default function SectionLectureSidebar(
    props: ISectionLectureSidebarProps,
) {
    const {
        course,
        contentCourse,
        handleContentCourse,
        handleResetFinishCourse,
    } = props;
    const indexLecture = {
        quiz: 1,
        article: 1,
        general: 0,
    };
    return (
        <div className={`relative flex bg-white transition-all first:flex-col`}>
            <div className="sticky left-0 top-0 flex bg-white">
                <div className="flex w-full items-center justify-between border border-primary-hover-gray py-2 pl-4 pr-2 text-base">
                    <h2 className="p-2 text-base font-bold text-primary-black">
                        Nội dung khóa học
                    </h2>
                    {/* <button className="flex items-center justify-between p-2">
                        <IoClose className="h-5 w-5" />
                    </button> */}
                </div>
            </div>
            <div className="max-h-[632px] flex-1 overflow-y-scroll ">
                {/* <div className="flex-1 overflow-y-auto overflow-x-hidden"> */}
                <AccordionController>
                    {course.curriculums.map((curriculum, indexCurriculum) => (
                        <AccordionItem
                            paddingPanel="p-4"
                            key={curriculum.id}
                            isOpen={
                                contentCourse?.curriculumID === curriculum.id
                                    ? true
                                    : false
                            }
                            positionIcon="end"
                            title={
                                <LecturePanel
                                    index={indexCurriculum}
                                    numberLecture={
                                        curriculum.lectures
                                            ? curriculum.lectures.length
                                            : 0
                                    }
                                    time={totalTimeLecture(
                                        curriculum.lectures
                                            ? curriculum.lectures
                                            : [],
                                    )}
                                    title={curriculum.title}
                                />
                            }
                        >
                            <ul className="border-t border-primary-hover-gray">
                                {curriculum.lectures &&
                                    curriculum.lectures.map(
                                        (lecture, index) => {
                                            const indexNumber =
                                                lecture.type === QUIZ_TYPE
                                                    ? indexLecture.quiz++
                                                    : indexLecture.article++;

                                            return (
                                                <LearningLectureItem
                                                    key={lecture.id}
                                                    handleContentCourse={
                                                        handleContentCourse
                                                    }
                                                    handleResetFinishCourse={
                                                        handleResetFinishCourse
                                                    }
                                                    indexList={
                                                        indexLecture.general++
                                                    }
                                                    curriculumID={curriculum.id}
                                                    index={indexNumber}
                                                    selected={
                                                        contentCourse
                                                            ? contentCourse.id ===
                                                              lecture.id
                                                            : false
                                                    }
                                                    lecture={lecture}
                                                    // handleSetVideoURL={
                                                    //     handleSetVideoURL
                                                    // }
                                                    type={
                                                        lecture.type ===
                                                        LECTURE_TYPE
                                                            ? lecture.article
                                                                ? "article"
                                                                : "video"
                                                            : "quiz"
                                                    }
                                                />
                                            );
                                        },
                                    )}
                            </ul>
                        </AccordionItem>
                    ))}
                    {/* {Array.from({ length: 8 }).map((_, i) => (
                        <AccordionItem
                            paddingPanel="p-4"
                            key={i}
                            isOpen={false}
                            positionIcon="end"
                            title={
                                <LecturePanel
                                    numberLecture={2}
                                    time="4min"
                                    title="Introduction"
                                />
                            }
                        >
                            <ul className="border-t border-primary-hover-gray">
                               
                            </ul>
                        </AccordionItem>
                    ))} */}
                </AccordionController>
            </div>
        </div>
    );
}
