import * as React from 'react';
import { FaFolderOpen } from 'react-icons/fa';
import { IoIosArrowDown } from 'react-icons/io';

export interface IResourceDropdownProps {
}

export default function ResourceDropdown (props: IResourceDropdownProps) {
  return (
      <div className="relative h-7 flex items-center border border-primary-black text-sm text-primary-gray">
          <div className="flex items-center px-2">
              <FaFolderOpen className='text-primary-black'/>
              <span className="ml-1">Resources</span>
              <IoIosArrowDown className="ml-1" />
          </div>
      </div>
  );
}
