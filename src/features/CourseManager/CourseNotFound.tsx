import { Button, Spinner } from "flowbite-react";
import { useEffect, useLayoutEffect, useState } from "react";
import { Link } from "react-router-dom";
import notFoundGif from "../assets/images/404.gif";

const CourseNotFound = () => {
 const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        // setIsLoading(false);
    }, []);

    //   const handleImageLoaded = () => {
    //       setIsLoading(false);
    //   };
    return (
        <div className="relative">
            {isLoading && (
                <div className="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                    <Spinner className="h-6 w-6" />
                </div>
            )}
            <div
                className={`${
                    isLoading ? "invisible" : "visible"
                } mx-10 mt-10 rounded bg-primary-black pb-10`}
            >
                <div className="">
                    <div className="flex flex-col items-center">
                        <div className="max-w-lg">
                            <img
                                src={
                                    "https://flowbite-admin-dashboard.vercel.app/images/illustrations/404.svg"
                                }
                                loading="lazy"
                                onLoad={() => {
                                    setIsLoading(false);
                                }}
                            />
                        </div>

                        <div className="text-[48px] font-bold text-white">
                            Không tìm thấy khóa học
                        </div>

                        <Button
                            className="mt-5 w-fit font-bold"
                            // outline
                            // gradientDuoTone="cyanToBlue"
                        >
                            <Link to="/courses">Danh sách khóa học</Link>
                        </Button>
                    </div>
                </div>
            </div>
            {/* )} */}
        </div>
    );
};

export default CourseNotFound;
