import {
    Badge,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import {
    REVIEW_INIT_STATUS,
    REVIEW_PENDING_STATUS,
    REVIEW_VERIFY_STATUS,
} from "../../../constants";
import { PaginationInfo } from "../../../models";
import { Course } from "../../../models/course";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { formatBetweenNumberPaginate } from "../../../utils";
import ResetReviewCourseModal from "./ResetReviewCourseModal";
// import {
//     AZ_FILTER,
//     NEWEST_FILTER,
//     OLDEST_FILTER,
//     ZA_FILTER,
// } from "../../constants";
// import { useAppSelector } from "../../hooks/redux-hook";
// import { Course, PaginationInfo } from "../../models";
// import { CourseServiceApi } from "../../services/api/courseServiceApi";
// import { selectUserAuth } from "../../services/state/redux/authSlide";
// import CourseGeneralItem from "./CourseGeneralItem";

// const TOTAL = 38;
// const TOTAL_PER_PAGE = 16;
// const TOTAL_PAGE = 3;
const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};
const SIZE = 10;
export default function CourseList() {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [openResetReview, setOpenResetReview] = React.useState(false);
    const [courseIDAction, setCourseIDAction] = React.useState<number | null>(
        null,
    );

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [courses, setCourse] = React.useState<Course[]>([]);
    // const [searchCourse, setSearchCourse] = React.useState("");
    // const [filter, setFilter] = React.useState(NEWEST_FILTER);
    const [loading, setLoading] = React.useState(false);

    const handleOpenResetReview = (command: boolean) => {
        setOpenResetReview(command);
        if (!command) {
            setCourseIDAction(null);
        }
    };

    const onPageChange = (page: number) => setCurrentPage(page);
    React.useLayoutEffect(() => {
        const fetchCourseGeneral = async () => {
            try {
                // await handle
                setLoading(true);
                const coursesResult = await CourseServiceApi.getCourse(
                    SIZE,
                    currentPage,
                );
                // if (infoPagination.totalItem == 0) {
                setInfoPagination({
                    totalPage: coursesResult.meta.pageCount,
                    totalItem: coursesResult.meta.itemCount,
                    size: coursesResult.meta.size,
                });
                // }
                setCourse(coursesResult.item);

                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseGeneral();
    }, [currentPage]);

    // const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     setSearchCourse(e.target.value);
    // };

    // const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
    //     setFilter(+e.target.value);
    // };
    const handleResetReviewCourse = async () => {
        try {
            if (courseIDAction) {
                const body = {
                    approval: 0,
                };
                await CourseServiceApi.approvalCourseReviews(
                    courseIDAction,
                    body,
                );
                setCourse((courses: Course[]) => {
                    const updateCourses = [...courses];
                    const courseIndex = updateCourses.findIndex(
                        (course) => course.id === courseIDAction,
                    );

                    if (courseIndex !== -1) {
                        const updateCourse = updateCourses[courseIndex];
                        updateCourse.reviewStatus = REVIEW_INIT_STATUS;

                        updateCourses[courseIndex] = updateCourse;
                        return updateCourses;
                    }

                    return courses;
                });
                handleOpenResetReview(false);

                toast(
                    <div className="font-bold">
                        Đặt lại đánh giá thành công!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            }
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Đặt lại đánh giá không thành công!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };
    return (
        <div className="">
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                <ResetReviewCourseModal
                    openModal={openResetReview}
                    handleOpenModal={handleOpenResetReview}
                    handleResetReviewCourse={handleResetReviewCourse}
                />
                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="text-center">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Tiêu đề
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Hình ảnh
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Trạng thái
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Hoạt động
                                </Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {courses.map((course, index) => (
                                    <Table.Row
                                        key={course.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {index +
                                                1 +
                                                (currentPage - 1) * SIZE}
                                        </Table.Cell>
                                        <Table.Cell className="max-w-[400px]  overflow-hidden text-ellipsis whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {course.title}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            <div className="flex justify-center">
                                                <img
                                                    className="h-[150px] w-[200px] border object-cover"
                                                    src={
                                                        course.image
                                                            ? course.image
                                                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                                                    }
                                                />
                                            </div>
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap  font-medium text-gray-900 dark:text-white">
                                            {course.reviewStatus ===
                                                REVIEW_INIT_STATUS && (
                                                <div className="flex justify-center">
                                                    <Badge
                                                        className="w-fit"
                                                        color="info"
                                                    >
                                                        Đánh giá ban đầu
                                                    </Badge>
                                                </div>
                                            )}
                                            {course.reviewStatus ===
                                                REVIEW_PENDING_STATUS && (
                                                <div className="flex justify-center">
                                                    <Badge
                                                        className="w-fit"
                                                        color="warning"
                                                    >
                                                        Đang chờ xem xét
                                                    </Badge>
                                                </div>
                                            )}
                                            {course.reviewStatus ===
                                                REVIEW_VERIFY_STATUS && (
                                                <div className="flex justify-center">
                                                    <Badge
                                                        className="w-fit"
                                                        color="success"
                                                    >
                                                        Đã được xác minh
                                                    </Badge>
                                                </div>
                                            )}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex justify-center gap-2">
                                                <Button
                                                    onClick={() => {
                                                        handleOpenResetReview(
                                                            true,
                                                        );
                                                        setCourseIDAction(
                                                            course.id,
                                                        );
                                                    }}
                                                    color="dark"
                                                >
                                                    Đặt lại đánh giá
                                                </Button>
                                                <Link
                                                    to={`/courses/${course.id}`}
                                                >
                                                    <Button>Chi tiết</Button>
                                                </Link>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!courses.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {infoPagination.totalItem
                                ? formatBetweenNumberPaginate(
                                      currentPage,
                                      infoPagination.size,
                                      infoPagination.totalItem,
                                  )
                                : 0}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
