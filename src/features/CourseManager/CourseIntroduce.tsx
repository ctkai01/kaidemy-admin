import { Card, List } from "flowbite-react";
import * as React from "react";
import { TiTick } from "react-icons/ti";
import { Link } from "react-router-dom";
import { Course, CourseDetailRes } from "../../models/course";
export interface ICourseIntroduceProps {
    course: CourseDetailRes;
}

export default function CourseIntroduce(props: ICourseIntroduceProps) {
    const { course } = props
    

    return (
        <div>
            <div className="flex gap-6">
                <div className="flex-1">
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Tiêu đề: </h2>
                        <div className="text-[#6b7280]">{course.title}</div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Phụ đề: </h2>
                        <div className="text-[#6b7280]">{course.subtitle}</div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Mô tả: </h2>
                        <div className="text-[#6b7280]">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: course.description
                                        ? course.description
                                        : "",
                                }}
                            ></div>
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Ngôn ngữ: </h2>
                        <div className="text-[#6b7280]">
                            {course.language?.name}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Cấp độ: </h2>
                        <div className="text-[#6b7280]">
                            {course.level?.name}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Danh mục: </h2>
                        <div className="text-[#6b7280]">
                            {course.category.name}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Danh mục con: </h2>
                        <div className="text-[#6b7280]">
                            {course.subCategory.name}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Được dạy chủ yếu:</h2>
                        <div className="text-[#6b7280]">
                            {course.primarilyTeach}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Bạn sẽ học được gì:</h2>
                        <div className="grid grid-cols-2">
                            {course.outComes?.map((item, index) => (
                                <div key={index} className="flex">
                                    <TiTick className="mr-2 h-4 w-4 text-green-500" />
                                    <div className="text-[#6b7280]">{item}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">Yêu cầu:</h2>
                        <div>
                            <List>
                                {course.requirements?.map((item, index) => (
                                    <List.Item key={index}>{item}</List.Item>
                                ))}
                            </List>
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="mb-1 font-bold">
                            Khóa học này dành cho ai:
                        </h2>
                        <div>
                            <List>
                                {course.intendedFor?.map((item, index) => (
                                    <List.Item key={index}>{item}</List.Item>
                                ))}
                            </List>
                        </div>
                    </div>
                </div>
                {/* 480x270 */}
                <div className="">
                    <Card
                        className="h-[270px] w-[280px] max-w-sm"
                        imgSrc={
                            course.image
                                ? course.image
                                : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                        }
                    >
                        <h5 className="text-center text-2xl font-bold tracking-tight text-green-800 dark:text-white">
                            $ {course.price ? course.price.value : 0}
                        </h5>
                        <div className="flex justify-end">
                            <span className="mr-1">Được tạo bởi</span>
                            <Link
                                className="font-bold text-cyan-500"
                                to={`http://localhost:5173/user/${course.user.id}`}
                            >
                                {course.user.name}
                            </Link>
                        </div>
                    </Card>
                </div>
            </div>
        </div>
    );
}
