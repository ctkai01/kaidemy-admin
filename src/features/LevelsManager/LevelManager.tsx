import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import { CreateLevel, Level, PaginationInfo, UpdateLevel } from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreateLevelModal from "./CreateLevelModal";
import DeleteLevelModal from "./DeleteLevelModal";
import UpdateLevelModal from "./UpdateLevelModal";

export interface ILevelManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function LevelManager(props: ILevelManagerProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [levels, setLevels] = React.useState<Level[]>([]);
    const [levelEdit, setLevelEdit] = React.useState<Level | null>(null);
    const [levelDelete, setLevelDelete] = React.useState<Level | null>(null);
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setLevelEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setLevelDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchLevels = async () => {
            try {
                const levelsResult = await CategoryServiceApi.getLevels(
                    10,
                    currentPage,
                );
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: levelsResult.meta.pageCount,
                        totalItem: levelsResult.meta.itemCount,
                        size: levelsResult.meta.size,
                    });
                }
                setLevels(levelsResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        setLoading(true);

        fetchLevels();
    }, [currentPage]);

    const handleCreateLevelData = async (data: CreateLevel) => {
        try {
            const body = {
                name: data.name,
            };
            const levelData = await CategoryServiceApi.createLevel(body);
            setLevels((levels) => [levelData, ...levels]);
            handleOpenCreateModal(false);
            setInfoPagination((pagination) => {
                let totalPage = pagination.totalPage;
                const totalUpdateItem = pagination.totalItem++;

                if (totalUpdateItem % 10 === 0) {
                    totalPage++;
                }
                return {
                    size: pagination.size,
                    totalItem: totalUpdateItem,
                    totalPage,
                };
            });
            toast(<div className="font-bold">Tạo thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateLevelData = async (data: UpdateLevel) => {
        try {
            const body = {
                name: data.name,
            };
            const levelData = await CategoryServiceApi.updateLevel(
                body,
                data.id,
            );

            setLevels((levels) => {
                const updateLevels = [...levels];
                const levelIndex = updateLevels.findIndex(
                    (level) => data.id === level.id,
                );

                if (levelIndex !== -1) {
                    updateLevels[levelIndex] = levelData;

                    return updateLevels;
                }

                return levels;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật thất bại!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };


    const handleDeleteLevelData = async () => {
        try {
            if (levelDelete) {
                await CategoryServiceApi.deleteLevel(levelDelete.id);
                setLevels((levels) => {
                    const updateLevels = [...levels];
                    const levelIndex = updateLevels.findIndex(
                        (level) => levelDelete.id === level.id,
                    );

                    if (levelIndex !== -1) {
                        updateLevels.splice(levelIndex, 1);
                        return updateLevels;
                    }

                    return levels;
                });
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });
                handleOpenDeleteModal(false);
                toast(<div className="font-bold">Xóa thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="">
            <CreateLevelModal
                openModal={openCreateModal}
                handleModal={handleOpenCreateModal}
                handleCreateLevelData={handleCreateLevelData}
            />
            {levelEdit && (
                <UpdateLevelModal
                    openModal={openUpdateModal}
                    handleModal={handleOpenUpdateModal}
                    handleUpdateLevelData={handleUpdateLevelData}
                    editLevel={levelEdit}
                />
            )}

            {
                <DeleteLevelModal
                    openModal={openDeleteModal}
                    handleModal={handleOpenDeleteModal}
                    handleDeleteLevelData={handleDeleteLevelData}
                />
            }

            <div
                onClick={() => handleOpenCreateModal(true)}
                className="mb-6 flex justify-end"
            >
                {" "}
                <Button className="mr-6" color="success">
                    Tạo
                </Button>
            </div>
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>Gia trị</Table.HeadCell>
                                <Table.HeadCell>Hành động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {levels.map((level, index) => (
                                    <Table.Row
                                        key={level.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {level.name}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setLevelEdit(level);
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Cập nhật
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        setLevelDelete(level);
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Xóa bỏ
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!levels.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
