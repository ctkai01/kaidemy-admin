import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { Level, UpdateLevel } from '../../models';
import { schemeUpdateLevel } from '../../validators/category';

export interface IUpdateLevelModalProps {
    openModal: boolean
    editLevel: Level | null
    handleModal: (command: boolean) => void
    handleUpdateLevelData: (data: UpdateLevel) => void
}

export default function UpdateLevelModal (props: IUpdateLevelModalProps) {
    const { openModal, editLevel, handleModal, handleUpdateLevelData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<UpdateLevel>({
        mode: "onChange",
        defaultValues: {
            id: editLevel ? editLevel.id : 0,
            name: editLevel ? editLevel.name : "",
        },
        resolver: yupResolver(schemeUpdateLevel),
    });
    
    const handleSubmitUpdateLevel = (data: UpdateLevel) => {
        handleUpdateLevelData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật cấp độ</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdateLevel)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Cập nhật</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
