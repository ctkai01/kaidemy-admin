import { Button, Modal } from 'flowbite-react';
import * as React from 'react';
import { HiOutlineExclamationCircle } from 'react-icons/hi';

export interface IDeleteLevelModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleDeleteLevelData: () => void
}

export default function DeleteLevelModal (props: IDeleteLevelModalProps) {
    const {openModal, handleModal, handleDeleteLevelData} = props
  return (
      <Modal
          show={openModal}
          size="md"
          onClose={() => handleModal(false)}
          popup
      >
          <Modal.Header />
          <Modal.Body>
              <div className="text-center">
                  <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                  <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                      Bạn có chắc chắn muốn xóa cấp độ này?
                  </h3>
                  <div className="flex justify-center gap-4">
                      <Button
                          color="failure"
                          onClick={() => handleDeleteLevelData()}
                      >
                          {"Vâng tôi chắc chắn"}
                      </Button>
                      <Button color="gray" onClick={() => handleModal(false)}>
                          Không, hủy bỏ
                      </Button>
                  </div>
              </div>
          </Modal.Body>
      </Modal>
  );
}
