import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { CreateLevel } from '../../models';
import { schemeCreateLevel } from '../../validators/category';

export interface ICreateLevelModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleCreateLevelData: (data: CreateLevel) => void
}

export default function CreateLevelModal (props: ICreateLevelModalProps) {
    const { openModal, handleModal, handleCreateLevelData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateLevel>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateLevel),
    });
    
    const handleSubmitCreateLevel = (data: CreateLevel) => {
        handleCreateLevelData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Tạo cấp độ</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreateLevel)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
