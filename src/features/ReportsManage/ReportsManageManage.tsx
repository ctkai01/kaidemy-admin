import {
    Avatar,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import { PaginationInfo } from "../../models";
import { Report } from "../../models/course";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { akaName, formatBetweenNumberPaginate } from "../../utils";

export interface IReportsManageProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function ReportsManage(props: IReportsManageProps) {
    const [currentPage, setCurrentPage] = React.useState(1);

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    const onPageChange = (page: number) => setCurrentPage(page);
    const [reports, setReports] = React.useState<Report[]>([]);
    const [loading, setLoading] = React.useState(false);

    React.useLayoutEffect(() => {
        const fetchCourseReviews = async () => {
            try {
                setLoading(true);
                const reportsResult = await CourseServiceApi.getReports(
                    10,
                    currentPage,
                );
                // if (infoPagination.totalItem == 0) {
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: reportsResult.meta.pageCount,
                        totalItem: reportsResult.meta.itemCount,
                        size: reportsResult.meta.size,
                    });
                }

                setReports(reportsResult.item);

                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };

        fetchCourseReviews();
    }, [currentPage]);

    return (
        <div className="">
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>
                                    Khóa học bị báo cáo
                                </Table.HeadCell>
                                <Table.HeadCell>Người báo cáo</Table.HeadCell>
                                <Table.HeadCell>Vấn đề báo cáo</Table.HeadCell>
                                <Table.HeadCell>Chi tiết</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {reports.map((report, index) => (
                                    <Table.Row
                                        key={report.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            <div className="flex flex-col justify-center">
                                                <img
                                                    className="w-[200px]"
                                                    src={
                                                        report.course.image
                                                            ? report.course
                                                                  .image
                                                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                                                    }
                                                />
                                                <Link
                                                    to={`http://localhost:5173/course/${report.course.id}`}
                                                    className="mt-2 font-bold"
                                                >
                                                    {report.course.title}
                                                </Link>
                                            </div>
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            <div className="flex flex-col">
                                                <div className="flex justify-start">
                                                    {report.user?.avatar ? (
                                                        <Avatar
                                                            img={
                                                                report.user
                                                                    ?.avatar
                                                            }
                                                            alt="avatar"
                                                            rounded
                                                            size="md"
                                                        />
                                                    ) : (
                                                        <Avatar
                                                            placeholderInitials={akaName(
                                                                report.user
                                                                    ?.name
                                                                    ? report
                                                                          .user
                                                                          ?.name
                                                                    : "",
                                                            )}
                                                            rounded
                                                            size="md"
                                                        />
                                                    )}
                                                </div>
                                                <Link
                                                    to={`http://localhost:5173/user/${report.user.id}`}
                                                    className="mt-2 font-bold"
                                                >
                                                    {report.user?.name}
                                                </Link>
                                            </div>
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {report.issueType.name}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {report.description}
                                        </Table.Cell>
                                        {/* <Table.Cell>
                                          <div className="flex gap-2">
                                              <Button
                                                  color="success"
                                                  onClick={() => {
                                                      handleOpenApprove(true);
                                                      setCourseIDAction(
                                                          course.id,
                                                      );
                                                  }}
                                              >
                                                  Chấp thuận
                                              </Button>
                                              <Button
                                                  onClick={() => {
                                                      handleOpenReject(true);
                                                      setCourseIDAction(
                                                          course.id,
                                                      );
                                                  }}
                                                  color="failure"
                                              >
                                                  Từ chối
                                              </Button>
                                              <Link
                                                  to={`/courses/${course.id}`}
                                              >
                                                  <Button>Chi tiết</Button>
                                              </Link>
                                          </div>
                                      </Table.Cell> */}
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!reports.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {infoPagination.totalItem
                                ? formatBetweenNumberPaginate(
                                      currentPage,
                                      infoPagination.size,
                                      infoPagination.totalItem,
                                  )
                                : 0}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
