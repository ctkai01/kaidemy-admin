import { Spinner } from 'flowbite-react';
import * as React from 'react';
import { OverviewAdmin } from '../../models/course';
import { CourseServiceApi } from '../../services/api/courseServiceApi';
import OverviewBox from './OverviewBox';

export interface IDashboardManagerProps {
}

export default function DashboardManager (props: IDashboardManagerProps) {
  const [overviewAdmin, setOverviewAdmin] =
      React.useState<OverviewAdmin | null>(null);
    React.useEffect(() => {
      const fetchOverview = async () => {
          try {
              const dataOverview = await CourseServiceApi.getOverviewAdmins();
              setOverviewAdmin(dataOverview);
          } catch (e) {
              console.log(e);
          }
      };
      fetchOverview();
  }, []);
  
    return (
        <div>
            <div className="px-16">
                <div className="mt-4">
                    {overviewAdmin ? (
                        <OverviewBox overviewAdmin={overviewAdmin} />
                    ) : (
                        <div className="text-center">
                            <Spinner />
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
