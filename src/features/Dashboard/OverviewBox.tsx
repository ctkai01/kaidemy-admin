import * as React from "react";
import { OverviewAdmin } from "../../models/course";
import CourseTab from "./CourseTab";
import RevenueTab from "./RevenueTab";
import UserTab from "./UserTab";
// import EnrollmentTab from "./EnrollmentTab";
// import RatingTab from "./RatingTab";

export interface IOverviewBoxProps {
    overviewAdmin: OverviewAdmin;
}

const OVERVIEW_TAB = {
    REVENUE: 2,
    COURSE: 1,
    USER: 0,
};

export default function OverviewBox(props: IOverviewBoxProps) {
    const [overviewTab, setOverviewTab] = React.useState(OVERVIEW_TAB.REVENUE);

    const { overviewAdmin } = props;
    // const handleChangeOverview = ()
    return (
        <div>
            <div className="flex border">
                <div
                    className={`cursor-pointer border-b border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.REVENUE &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.REVENUE);
                    }}
                >
                    <div className=" text-primary-gray">Doanh thu</div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewAdmin.revenue.platform.total} $
                    </div>
                    <div className=" text-primary-gray">
                        {overviewAdmin.revenue.platform.totalThisMonth} $ trong
                        tháng này
                    </div>
                </div>
                <div
                    className={`cursor-pointer border-b border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.COURSE &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.COURSE);
                    }}
                >
                    <div className=" text-primary-gray">Tổng số khóa học</div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewAdmin.courses.total}
                    </div>
                    <div className=" text-primary-gray">
                        {overviewAdmin.courses.totalThisMonth} trong tháng này
                    </div>
                </div>
                <div
                    className={`cursor-pointer border-b border-t border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.USER &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.USER);
                    }}
                >
                    <div className=" text-primary-gray">Người dùng</div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewAdmin.users.total}
                    </div>
                    <div className=" text-primary-gray">
                        {overviewAdmin.users.totalThisMonth} trong tháng này
                    </div>
                </div>
            </div>
            <div className="h-[300px]">
                {overviewTab === OVERVIEW_TAB.COURSE && (
                    <CourseTab overviewAdmin={overviewAdmin} />
                )}

                {overviewTab === OVERVIEW_TAB.USER && (
                    <UserTab overviewAdmin={overviewAdmin} />
                )}

                {overviewTab === OVERVIEW_TAB.REVENUE && (
                    <RevenueTab overviewAdmin={overviewAdmin} />
                )}
            </div>
        </div>
    );
}
