import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import {
    CreateIssueType,
    IssueType,
    PaginationInfo,
    UpdateIssueType,
} from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreateIssueTypeModal from "./CreateIssueTypeModal";
import DeleteIssueTypeModal from "./DeleteIssueTypeModal";
import UpdateIssueTypeModal from "./UpdateIssueTypeModal";

export interface IIssueTypeManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function IssueTypeManager(props: IIssueTypeManagerProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [issueTypes, setIssueTypes] = React.useState<IssueType[]>([]);
    const [issueTypeEdit, setIssueTypeEdit] = React.useState<IssueType | null>(
        null,
    );
    const [issueTypeDelete, setIssueTypeDelete] =
        React.useState<IssueType | null>(null);
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setIssueTypeEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setIssueTypeDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchIssueTypes = async () => {
            try {
                const issueTypesResult = await CategoryServiceApi.getIssueTypes(
                    10,
                    currentPage,
                );
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: issueTypesResult.meta.pageCount,
                        totalItem: issueTypesResult.meta.itemCount,
                        size: issueTypesResult.meta.size,
                    });
                }
                setIssueTypes(issueTypesResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        setLoading(true);

        fetchIssueTypes();
    }, [currentPage]);

    const handleCreateIssueTypeData = async (data: CreateIssueType) => {
        try {
            const body = {
                name: data.name,
            };

            const issueTypeData =
                await CategoryServiceApi.createIssueType(body);
            setIssueTypes((issueTypes) => [...issueTypes, issueTypeData]);
            handleOpenCreateModal(false);
            setInfoPagination((pagination) => {
                let totalPage = pagination.totalPage;
                const totalUpdateItem = pagination.totalItem++;

                if (totalUpdateItem % 10 === 0) {
                    totalPage++;
                }
                return {
                    size: pagination.size,
                    totalItem: totalUpdateItem,
                    totalPage,
                };
            });
            toast(<div className="font-bold">Tạo thành công!!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateIssueTypeData = async (data: UpdateIssueType) => {
        try {
            const body = {
                name: data.name,
            };
            const issueTypeData = await CategoryServiceApi.updateIssueType(
                body,
                data.id,
            );

            setIssueTypes((issueTypes) => {
                const updateIssueTypes = [...issueTypes];
                const issueTypeIndex = updateIssueTypes.findIndex(
                    (issueType) => data.id === issueType.id,
                );

                if (issueTypeIndex !== -1) {
                    updateIssueTypes[issueTypeIndex] = issueTypeData;

                    return updateIssueTypes;
                }

                return issueTypes;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };


    const handleDeleteIssueTypeData = async () => {
        try {
            if (issueTypeDelete) {
                await CategoryServiceApi.deleteIssueType(issueTypeDelete.id);
                setIssueTypes((issueTypes) => {
                    const updateIssueTypes = [...issueTypes];
                    const issueTypeIndex = updateIssueTypes.findIndex(
                        (issueType) => issueTypeDelete.id === issueType.id,
                    );

                    if (issueTypeIndex !== -1) {
                        updateIssueTypes.splice(issueTypeIndex, 1);
                        return updateIssueTypes;
                    }

                    return issueTypes;
                });
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });
                handleOpenDeleteModal(false);
                toast(<div className="font-bold">Xóa thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="">
            <CreateIssueTypeModal
                openModal={openCreateModal}
                handleModal={handleOpenCreateModal}
                handleCreateIssueTypeData={handleCreateIssueTypeData}
            />
            {issueTypeEdit && (
                <UpdateIssueTypeModal
                    openModal={openUpdateModal}
                    handleModal={handleOpenUpdateModal}
                    handleUpdateIssueTypeData={handleUpdateIssueTypeData}
                    editIssueType={issueTypeEdit}
                />
            )}

            {
                <DeleteIssueTypeModal
                    openModal={openDeleteModal}
                    handleModal={handleOpenDeleteModal}
                    handleDeleteIssueTypeData={handleDeleteIssueTypeData}
                />
            }

            <div
                onClick={() => handleOpenCreateModal(true)}
                className="mb-6 flex justify-end"
            >
                {" "}
                <Button className="mr-6" color="success">
                    Tạo
                </Button>
            </div>
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>Giá trị</Table.HeadCell>
                                <Table.HeadCell>Hành động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {issueTypes.map((issueType, index) => (
                                    <Table.Row
                                        key={issueType.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {issueType.name}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setIssueTypeEdit(
                                                            issueType,
                                                        );
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Cập nhật
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        setIssueTypeDelete(
                                                            issueType,
                                                        );
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Xóa bỏ
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!issueTypes.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
