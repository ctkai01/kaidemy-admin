import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { CreateIssueType } from '../../models';
import { schemeCreateIssueType } from '../../validators/category';

export interface ICreateIssueTypeModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleCreateIssueTypeData: (data: CreateIssueType) => void
}

export default function CreateIssueTypeModal (props: ICreateIssueTypeModalProps) {
    const { openModal, handleModal, handleCreateIssueTypeData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateIssueType>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateIssueType),
    });
    
    const handleSubmitCreateIssueType = (data: CreateIssueType) => {
        handleCreateIssueTypeData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Tạo loại vấn đề</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreateIssueType)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
