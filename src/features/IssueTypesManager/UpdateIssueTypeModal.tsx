import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { IssueType, Language, UpdateIssueType, UpdateLanguage } from '../../models';
import { schemeUpdateIssueType, schemeUpdateLanguage } from '../../validators/category';

export interface IUpdateIssueTypeModalProps {
    openModal: boolean
    editIssueType: IssueType | null
    handleModal: (command: boolean) => void
    handleUpdateIssueTypeData: (data: UpdateIssueType) => void
}

export default function UpdateIssueTypeModal (props: IUpdateIssueTypeModalProps) {
    const { openModal, editIssueType, handleModal, handleUpdateIssueTypeData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<UpdateIssueType>({
        mode: "onChange",
        defaultValues: {
            id: editIssueType ? editIssueType.id : 0,
            name: editIssueType ? editIssueType.name : "",
        },
        resolver: yupResolver(schemeUpdateIssueType),
    });
    
    const handleSubmitUpdateIssueType = (data: UpdateIssueType) => {
        handleUpdateIssueTypeData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật loại vấn đề</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdateIssueType)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Cập nhật</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
