import {
    Badge,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Select,
    Spinner,
    Table,
    TextInput,
} from "flowbite-react";
import * as React from "react";
import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import {
    ALL,
    BLOCK_FILTER,
    NORMAL_FILTER,
    NORMAL_ROLE,
    NOT_BLOCK_FILTER,
    REVIEW_INIT_STATUS,
    REVIEW_PENDING_STATUS,
    REVIEW_VERIFY_STATUS,
    TEACHER_FILTER,
    TEACHER_ROLE,
} from "../../constants";
import { PaginationInfo, User } from "../../models";
import { Course } from "../../models/course";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { UserServiceApi } from "../../services/api/userServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import BlockUserModal from "./BLockUserModal";
import UnBLockUserModal from "./UnBLockUserModal";

// import {
//     AZ_FILTER,
//     NEWEST_FILTER,
//     OLDEST_FILTER,
//     ZA_FILTER,
// } from "../../constants";
// import { useAppSelector } from "../../hooks/redux-hook";
// import { Course, PaginationInfo } from "../../models";
// import { CourseServiceApi } from "../../services/api/courseServiceApi";
// import { selectUserAuth } from "../../services/state/redux/authSlide";
// import CourseGeneralItem from "./CourseGeneralItem";

// const TOTAL = 38;
// const TOTAL_PER_PAGE = 16;
// const TOTAL_PAGE = 3;
const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

const SIZE = 3;
export default function UserManage() {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [openBlockUser, setOpenBlockUser] = React.useState(false);
    const [openUnBlockUser, setOpenUnBlockUser] = React.useState(false);
    const [userIDAction, setUserIDAction] = React.useState<number | null>(null);

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [users, setUsers] = React.useState<User[]>([]);
    const [searchCourse, setSearchCourse] = React.useState("");
    const [filter, setFilter] = React.useState(ALL);
    const [loading, setLoading] = React.useState(false);

    const handleOpenBlockUser = (command: boolean) => {
        setOpenBlockUser(command);
        if (!command) {
            setUserIDAction(null);
        }
    };

    const handleOpenUnBlockUser = (command: boolean) => {
        setOpenUnBlockUser(command);
        if (!command) {
            setUserIDAction(null);
        }
    };

    const onPageChange = (page: number) => setCurrentPage(page);
    React.useLayoutEffect(() => {
        const fetchCourseGeneral = async () => {
            try {
                // await handle
                setLoading(true);
                const usersResult = await UserServiceApi.getUsers(
                    SIZE,
                    currentPage,
                    searchCourse ? searchCourse : null,
                    filter,
                );
                // if (infoPagination.totalItem == 0) {

                setInfoPagination({
                    totalPage: usersResult.meta.pageCount,
                    totalItem: usersResult.meta.itemCount,
                    size: usersResult.meta.size,
                });
                // }
                setUsers(usersResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseGeneral();
    }, [currentPage, searchCourse, filter]);

    // const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     setSearchCourse(e.target.value);
    // };

    // const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
    //     setFilter(+e.target.value);
    // };
    const handleBlockUser = async () => {
        try {
            if (userIDAction) {
                const body = {
                    isBlock: 1,
                };
                await UserServiceApi.blockUser(body, userIDAction);
                setUsers((users: User[]) => {
                    const updateUsers = [...users];
                    const userIndex = updateUsers.findIndex(
                        (course) => course.id === userIDAction,
                    );

                    if (userIndex !== -1) {
                        const updateCourse = updateUsers[userIndex];
                        updateCourse.isBlock = true;

                        updateUsers[userIndex] = updateCourse;
                        return updateUsers;
                    }

                    return users;
                });
                handleOpenBlockUser(false);

                toast(
                    <div className="font-bold">
                        Chặn người dùng thành công!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            }
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Chặn người dùng không thành công!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleUnBlockUser = async () => {
        try {
            if (userIDAction) {
                const body = {
                    isBlock: 0,
                };
                await UserServiceApi.blockUser(body, userIDAction);
                setUsers((users: User[]) => {
                    const updateUsers = [...users];
                    const userIndex = updateUsers.findIndex(
                        (course) => course.id === userIDAction,
                    );

                    if (userIndex !== -1) {
                        const updateCourse = updateUsers[userIndex];
                        updateCourse.isBlock = false;

                        updateUsers[userIndex] = updateCourse;
                        return updateUsers;
                    }

                    return users;
                });
                handleOpenUnBlockUser(false);

                toast(
                    <div className="font-bold">
                        Bỏ chặn người dùng thành công!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            }
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Bỏ chặn người dùng không thành công!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchCourse(e.target.value);
    };

    const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setFilter(+e.target.value);
        setCurrentPage(1);
    };
    return (
        <div className="">
            <div className="overflow-x-auto ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                <BlockUserModal
                    openModal={openBlockUser}
                    handleOpenModal={handleOpenBlockUser}
                    handleBlockUser={handleBlockUser}
                />
                <UnBLockUserModal
                    openModal={openUnBlockUser}
                    handleOpenModal={handleOpenUnBlockUser}
                    handleUnBlockUser={handleUnBlockUser}
                />
                <div className="mb-8">
                    <div className="ml-4 flex gap-8">
                        <TextInput
                            type="text"
                            rightIcon={FaSearch}
                            onChange={handleChangeSearch}
                            placeholder="Tìm kiếm người dùng"
                        />

                        <Select onChange={handleChangeFilter}>
                            <option value={ALL}>Tất cả</option>
                            <option value={TEACHER_FILTER}>Giảng viên</option>
                            <option value={NORMAL_FILTER}>
                                Người dùng bình thường
                            </option>
                            <option value={BLOCK_FILTER}>Bị chặn</option>
                            <option value={NOT_BLOCK_FILTER}>
                                Không bị chặn
                            </option>
                        </Select>
                    </div>
                </div>
                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="text-center">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Tên
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Email
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Hình đại diện
                                </Table.HeadCell>
                                <Table.HeadCell className="text-center">
                                    Vai trò
                                </Table.HeadCell>
                                {/*
                                <Table.HeadCell className="text-center">
                                    Status
                                </Table.HeadCell> */}
                                <Table.HeadCell className="text-center">
                                    Hoạt động
                                </Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {users.map((user, index) => (
                                    <Table.Row
                                        key={user.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap text-center font-medium text-gray-900 dark:text-white">
                                            {index +
                                                1 +
                                                (currentPage - 1) * SIZE}
                                        </Table.Cell>
                                        <Table.Cell className="max-w-[400px] overflow-hidden text-ellipsis whitespace-nowrap text-center font-medium text-gray-900 dark:text-white">
                                            {user.name}
                                        </Table.Cell>
                                        <Table.Cell className="max-w-[400px] overflow-hidden text-ellipsis whitespace-nowrap text-center font-medium text-gray-900 dark:text-white">
                                            {user.email}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            <div className="flex justify-center">
                                                <img
                                                    className="h-[150px] w-[200px] border object-cover"
                                                    src={
                                                        user.avatar
                                                            ? user.avatar
                                                            : "https://static.vecteezy.com/system/resources/previews/026/619/142/non_2x/default-avatar-profile-icon-of-social-media-user-photo-image-vector.jpg"
                                                    }
                                                />
                                            </div>
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap  font-medium text-gray-900 dark:text-white">
                                            {user.role === NORMAL_ROLE && (
                                                <div className="flex justify-center">
                                                    <Badge
                                                        className="w-fit"
                                                        color="info"
                                                    >
                                                        Người dùng thường
                                                    </Badge>
                                                </div>
                                            )}
                                            {user.role === TEACHER_ROLE && (
                                                <div className="flex justify-center">
                                                    <Badge
                                                        className="w-fit"
                                                        color="warning"
                                                    >
                                                        Giảng viên
                                                    </Badge>
                                                </div>
                                            )}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex justify-center gap-2">
                                                {user.isBlock ? (
                                                    <Button
                                                        onClick={() => {
                                                            handleOpenUnBlockUser(
                                                                true,
                                                            );
                                                            setUserIDAction(
                                                                user.id,
                                                            );
                                                        }}
                                                        color="success"
                                                    >
                                                        Bỏ chặn
                                                    </Button>
                                                ) : (
                                                    <Button
                                                        onClick={() => {
                                                            handleOpenBlockUser(
                                                                true,
                                                            );
                                                            setUserIDAction(
                                                                user.id,
                                                            );
                                                        }}
                                                        color="failure"
                                                    >
                                                        Chặn
                                                    </Button>
                                                )}

                                                <Link
                                                    // to={`/courses/${course.id}`}
                                                    to={`http://localhost:5173/user/${user.id}`}
                                                >
                                                    <Button>Chi tiết</Button>
                                                </Link>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!users.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
