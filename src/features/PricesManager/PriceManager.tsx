import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { HiShoppingCart } from "react-icons/hi";
import { toast } from "react-toastify";
import { CreatePrice, PaginationInfo, Price, UpdatePrice } from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreatePriceModal from "./CreatePriceModal";
import DeletePriceModal from "./DeletePriceModal";
import UpdatePriceModal from "./UpdatePriceModal";

export interface IPriceManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};
export default function PriceManager(props: IPriceManagerProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [prices, setPrices] = React.useState<Price[]>([]);
    const [priceEdit, setPriceEdit] = React.useState<Price | null>(null);
    const [priceDelete, setPriceDelete] = React.useState<Price | null>(null);
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setPriceEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setPriceDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchPrices = async () => {
            try {
                const pricesResult = await CategoryServiceApi.getPrices(
                    10,
                    currentPage,
                );
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: pricesResult.meta.pageCount,
                        totalItem: pricesResult.meta.itemCount,
                        size: pricesResult.meta.size,
                    });
                }
                setPrices(pricesResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        setLoading(true);

        fetchPrices();
    }, [currentPage]);

    const handleCreatePriceData = async (data: CreatePrice) => {
        try {
            const body = {
                tier: data.tier,
                value: data.value,
            };
            const priceData = await CategoryServiceApi.createPrice(body);
            setPrices((prices) => [...prices, priceData]);
            handleOpenCreateModal(false);
            setInfoPagination((pagination) => {
                let totalPage = pagination.totalPage;
                const totalUpdateItem = pagination.totalItem++;

                if (totalUpdateItem % 10 === 0) {
                    totalPage++;
                }
                return {
                    size: pagination.size,
                    totalItem: totalUpdateItem,
                    totalPage,
                };
            });
            toast(<div className="font-bold">Tạo thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdatePriceData = async (data: UpdatePrice) => {
        try {
            const body = {
                tier: data.tier,
                value: data.value,
            };
            const priceData = await CategoryServiceApi.updatePrice(
                body,
                data.id,
            );

            setPrices((prices) => {
                const updatePrices = [...prices];
                const priceIndex = updatePrices.findIndex(
                    (price) => data.id === price.id,
                );

                if (priceIndex !== -1) {
                    updatePrices[priceIndex] = priceData;

                    return updatePrices;
                }

                return prices;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };


    const handleDeletePriceData = async () => {
        try {
            if (priceDelete) {
                await CategoryServiceApi.deletePrice(priceDelete.id);
                setPrices((prices) => {
                    const updatePrices = [...prices];
                    const priceIndex = updatePrices.findIndex(
                        (price) => priceDelete.id === price.id,
                    );

                    if (priceIndex !== -1) {
                        updatePrices.splice(priceIndex, 1);
                        return updatePrices;
                    }

                    return prices;
                });
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });
                handleOpenDeleteModal(false);
                toast(<div className="font-bold">Xóa thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="">
            <CreatePriceModal
                openModal={openCreateModal}
                handleModal={handleOpenCreateModal}
                handleCreatePriceData={handleCreatePriceData}
            />
            {priceEdit && (
                <UpdatePriceModal
                    openModal={openUpdateModal}
                    handleModal={handleOpenUpdateModal}
                    handleUpdatePriceData={handleUpdatePriceData}
                    editPrice={priceEdit}
                />
            )}

            {
                <DeletePriceModal
                    openModal={openDeleteModal}
                    handleModal={handleOpenDeleteModal}
                    handleDeletePriceData={handleDeletePriceData}
                />
            }

            <div
                onClick={() => handleOpenCreateModal(true)}
                className="mb-6 flex justify-end"
            >
                {" "}
                <Button className="mr-6" color="success">
                    Tạo
                </Button>
            </div>
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell>STT</Table.HeadCell>

                                <Table.HeadCell>Loại</Table.HeadCell>
                                <Table.HeadCell>Giá trị</Table.HeadCell>
                                <Table.HeadCell>Hành động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {prices.map((price, index) => (
                                    <Table.Row
                                        key={price.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {price.tier}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            ${price.value}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setPriceEdit(price);
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Cập nhật
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        setPriceDelete(price);
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Xóa bỏ
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!prices.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
