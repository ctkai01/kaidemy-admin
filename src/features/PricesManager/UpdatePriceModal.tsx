import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import * as React from 'react';
import { useForm } from 'react-hook-form';
import { CreatePrice, Price, UpdatePrice } from '../../models';
import { schemeCreatePrice, schemeUpdatePrice } from '../../validators/category';

export interface IUpdatePriceModalProps {
    openModal: boolean
    editPrice: Price | null
    handleModal: (command: boolean) => void
    handleUpdatePriceData: (data: UpdatePrice) => void
}

export default function UpdatePriceModal (props: IUpdatePriceModalProps) {
    const { openModal, editPrice, handleModal, handleUpdatePriceData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<UpdatePrice>({
        mode: "onChange",
        defaultValues: {
            id: editPrice ? editPrice.id : 0,
            tier: editPrice ? editPrice.tier : "",
            value: editPrice ? editPrice.value : 0,
        },
        resolver: yupResolver(schemeUpdatePrice),
    });
    
    const handleSubmitUpdatePrice = (data: UpdatePrice) => {
        handleUpdatePriceData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật cấp độ</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdatePrice)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="tier" value="Loại" />
                        </div>
                        <TextInput
                            id="tier"
                            type="text"
                            color={errors.tier ? "failure" : ""}
                            {...register("tier")}
                            helperText={
                                <>
                                    {errors.tier ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.tier.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-4">
                        <div className="mb-2 block">
                            <Label htmlFor="value" value="Giá trị" />
                        </div>
                        <TextInput
                            id="value"
                            type="text"
                            color={errors.value ? "failure" : ""}
                            {...register("value")}
                            helperText={
                                <>
                                    {errors.value ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.value.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Cập nhật</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
