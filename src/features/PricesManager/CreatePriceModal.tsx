import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import * as React from 'react';
import { useForm } from 'react-hook-form';
import { CreatePrice } from '../../models';
import { schemeCreatePrice } from '../../validators/category';

export interface ICreatePriceModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleCreatePriceData: (data: CreatePrice) => void
}

export default function CreatePriceModal (props: ICreatePriceModalProps) {
    const { openModal, handleModal, handleCreatePriceData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreatePrice>({
        mode: "onChange",
        resolver: yupResolver(schemeCreatePrice),
    });
    
    const handleSubmitCreatePrice = (data: CreatePrice) => {
        handleCreatePriceData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Tạo cấp độ</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreatePrice)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="tier" value="Loại" />
                        </div>
                        <TextInput
                            id="tier"
                            type="text"
                            color={errors.tier ? "failure" : ""}
                            {...register("tier")}
                            helperText={
                                <>
                                    {errors.tier ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.tier.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-4">
                        <div className="mb-2 block">
                            <Label htmlFor="value" value="Giá trị" />
                        </div>
                        <TextInput
                            id="value"
                            type="text"
                            color={errors.value ? "failure" : ""}
                            {...register("value")}
                            helperText={
                                <>
                                    {errors.value ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.value.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
