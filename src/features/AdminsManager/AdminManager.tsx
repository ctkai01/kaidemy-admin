import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import {
    CreateAdmin,
    CreateLanguage,
    Language,
    PaginationInfo,
    UpdateAdmin,
    UpdateLanguage,
    User,
} from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { UserServiceApi } from "../../services/api/userServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreateAdminModal from "./CreateAdminModal";
import CreateLanguageModal from "./CreateAdminModal";
import DeleteAdminModal from "./DeleteAdminModal";
import DeleteLanguageModal from "./DeleteAdminModal";
import UpdateAdminModal from "./UpdateAdminModal";
import UpdateLanguageModal from "./UpdateAdminModal";

export interface IAdminManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function AdminManager(props: IAdminManagerProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [admins, setAdmins] = React.useState<User[]>([]);
    const [adminEdit, setAdminEdit] = React.useState<User | null>(null);
    const [adminDelete, setAdminDelete] = React.useState<User | null>(null);
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setAdminEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setAdminDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchAdmins = async () => {
            try {
                const adminsResult = await UserServiceApi.getNormalAdmins(
                    10,
                    currentPage,
                );
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: adminsResult.meta.pageCount,
                        totalItem: adminsResult.meta.itemCount,
                        size: adminsResult.meta.size,
                    });
                }
                setAdmins(adminsResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        setLoading(true);

        fetchAdmins();
    }, [currentPage]);

    const handleCreateAdminData = async (data: CreateAdmin) => {
        try {
            const body = {
                name: data.name,
                email: data.email,
                password: data.password,
            };

            const adminData = await UserServiceApi.createNormalAdmin(body);
            setAdmins((admins) => [adminData, ...admins]);
            handleOpenCreateModal(false);
            setInfoPagination((pagination) => {
                let totalPage = pagination.totalPage;
                const totalUpdateItem = pagination.totalItem++;

                if (totalUpdateItem % 10 === 0) {
                    totalPage++;
                }
                return {
                    size: pagination.size,
                    totalItem: totalUpdateItem,
                    totalPage,
                };
            });
            toast(<div className="font-bold">Tạo thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateAdminData = async (data: UpdateAdmin) => {
        try {
            const body: any = {};
            if (data.name) {
                body.name = data.name;
            }
            if (data.password) {
                body.password = data.password;
            }
            const adminData = await UserServiceApi.updateNormalAdmin(
                body,
                data.id,
            );

            setAdmins((admins) => {
                const updateAdmins = [...admins];
                const adminIndex = updateAdmins.findIndex(
                    (admin) => data.id === admin.id,
                );

                if (adminIndex !== -1) {
                    updateAdmins[adminIndex] = adminData;

                    return updateAdmins;
                }

                return admins;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteAdminData = async () => {
        try {
            if (adminDelete) {
                await UserServiceApi.deleteNormalAdmin(adminDelete.id);
                setAdmins((admins) => {
                    const updateAdmins = [...admins];
                    const adminIndex = updateAdmins.findIndex(
                        (admin) => adminDelete.id === admin.id,
                    );

                    if (adminIndex !== -1) {
                        updateAdmins.splice(adminIndex, 1);
                        return updateAdmins;
                    }

                    return admins;
                });

                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });

                handleOpenDeleteModal(false);
                toast(<div className="font-bold">Xóa thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    return (
        <div className="">
            <CreateAdminModal
                openModal={openCreateModal}
                handleModal={handleOpenCreateModal}
                handleCreateAdminData={handleCreateAdminData}
            />
            {adminEdit && (
                <UpdateAdminModal
                    openModal={openUpdateModal}
                    handleModal={handleOpenUpdateModal}
                    handleUpdateAdminData={handleUpdateAdminData}
                    editAdmin={adminEdit}
                />
            )}

            {
                <DeleteAdminModal
                    openModal={openDeleteModal}
                    handleModal={handleOpenDeleteModal}
                    handleDeleteAdminData={handleDeleteAdminData}
                />
            }

            <div
                onClick={() => handleOpenCreateModal(true)}
                className="mb-6 flex justify-end"
            >
                {" "}
                <Button className="mr-6" color="success">
                    Tạo
                </Button>
            </div>
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>Email</Table.HeadCell>
                                <Table.HeadCell>Tên</Table.HeadCell>
                                <Table.HeadCell>Hành động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {admins.map((admin, index) => (
                                    <Table.Row
                                        key={admin.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {admin.email}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {admin.name}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setAdminEdit(admin);
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Cập nhật
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        setAdminDelete(admin);
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Xóa
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!admins.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
