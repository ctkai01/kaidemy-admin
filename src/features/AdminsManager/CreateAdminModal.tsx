import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { CreateAdmin, CreateLanguage } from '../../models';
import { schemeCreateAdmin } from '../../validators/auth';
import { schemeCreateLanguage } from '../../validators/category';

export interface ICreateAdminModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleCreateAdminData: (data: CreateAdmin) => void
}

export default function CreateAdminModal (props: ICreateAdminModalProps) {
    const { openModal, handleModal, handleCreateAdminData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateAdmin>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateAdmin),
    });
    
    const handleSubmitCreateAdmin = (data: CreateAdmin) => {
        handleCreateAdminData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Tạo quản trị viên</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreateAdmin)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="email" value="Email" />
                        </div>
                        <TextInput
                            id="email"
                            type="text"
                            color={errors.email ? "failure" : ""}
                            {...register("email")}
                            helperText={
                                <>
                                    {errors.email ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.email.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="password" value="Mật khẩu" />
                        </div>
                        <TextInput
                            id="password"
                            type="password"
                            color={errors.password ? "failure" : ""}
                            {...register("password")}
                            helperText={
                                <>
                                    {errors.password ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>
                                                {errors.password.message}
                                            </span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
