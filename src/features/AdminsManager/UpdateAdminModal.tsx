import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Language, UpdateAdmin, UpdateLanguage, User } from '../../models';
import { schemeUpdateAdmin } from '../../validators/auth';
import { schemeUpdateLanguage } from '../../validators/category';

export interface IUpdateAdminModalProps {
    openModal: boolean
    editAdmin: User | null
    handleModal: (command: boolean) => void
    handleUpdateAdminData: (data: UpdateAdmin) => void
}

export default function UpdateAdminModal (props: IUpdateAdminModalProps) {
    const { openModal, editAdmin, handleModal, handleUpdateAdminData} = props
    const [isResetPassword, setIsResetPassword] = useState(false)
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<UpdateAdmin>({
        mode: "onChange",
        defaultValues: {
            id: editAdmin ? editAdmin.id : 0,
            name: editAdmin ? editAdmin.name : "",
        },
        resolver: yupResolver(schemeUpdateAdmin),
    });
    
    const handleSubmitUpdateLanguage = (data: UpdateAdmin) => {
        handleUpdateAdminData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật quản trị viên</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdateLanguage)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    {!isResetPassword && (
                        <Button onClick={() => setIsResetPassword(true)}>
                            Đặt lại mật khẩu
                        </Button>
                    )}
                    {isResetPassword && (
                        <div>
                            <div className="mb-2 block">
                                <Label
                                    htmlFor="password"
                                    value="Đặt lại mật khẩu"
                                />
                            </div>
                            <TextInput
                                id="password"
                                type="text"
                                color={errors.password ? "failure" : ""}
                                {...register("password")}
                                helperText={
                                    <>
                                        {errors.password ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.password.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            />
                        </div>
                    )}

                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Cập nhật</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
