import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { SignIn } from "../../../models";
import { schemeSignIn } from "../../../validators/auth";
export interface ILoginFormProps {
    handleLogin: (data: SignIn) => void
}

export default function LoginForm(props: ILoginFormProps) {

    const {handleLogin} = props
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<SignIn>({
        mode: "onChange",
        resolver: yupResolver(schemeSignIn),
    });


    const handleSignIn = (data: SignIn) => {
        handleLogin(data)
    }
  return (
      <form
          onSubmit={handleSubmit(handleSignIn)}
          className="flex max-w-md flex-col gap-4"
      >
          <div>
              <div className="mb-2 block">
                  <Label htmlFor="email1" value="Email" />
              </div>
              <TextInput
                  id="email1"
                  type="text"
                  color={errors.email ? "failure" : ""}
                  {...register("email")}
                  helperText={
                      <>
                          {errors.email ? (
                              <>
                                  <span className="font-medium">Oops! </span>
                                  <span>{errors.email.message}</span>
                              </>
                          ) : (
                              <></>
                          )}
                      </>
                  }
              />
          </div>
          <div>
              <div className="mb-2 block">
                  <Label htmlFor="password1" value="Mật khẩu" />
              </div>
              <TextInput
                  color={errors.password ? "failure" : ""}
                  id="password1"
                  type="password"
                  {...register("password")}
                  helperText={
                      <>
                          {errors.password ? (
                              <>
                                  <span className="font-medium">Oops! </span>
                                  <span>{errors.password.message}</span>
                              </>
                          ) : (
                              <></>
                          )}
                      </>
                  }
              />
          </div>
          <Button type="submit">Đăng nhập</Button>
      </form>
  );
}
