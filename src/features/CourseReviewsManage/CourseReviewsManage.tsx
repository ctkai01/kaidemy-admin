import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { TiTick } from "react-icons/ti";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { REVIEW_INIT_STATUS, REVIEW_VERIFY_STATUS } from "../../constants";
import { PaginationInfo } from "../../models";
import { Course } from "../../models/course";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import ApproveCourseModal from "./ApproveCourseModal";
import RejectCourseModal from "./RejectCourseModal";

export interface ICourseReviewsManageProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function CourseReviewsManage(props: ICourseReviewsManageProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [openApprove, setOpenApprove] = React.useState(false);
    const [openReject, setOpenReject] = React.useState(false);
    const [courseIDAction, setCourseIDAction] = React.useState<number | null>(
        null,
    );

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    const onPageChange = (page: number) => setCurrentPage(page);
    const [courseReviews, setCourseReviews] = React.useState<Course[]>([]);
    const [loading, setLoading] = React.useState(false);

    const handleOpenApprove = (command: boolean) => {
        setOpenApprove(command);
        if (!command) {
            setCourseIDAction(null);
        }
    };

    const handleOpenReject = (command: boolean) => {
        setOpenReject(command);
        if (!command) {
            setCourseIDAction(null);
        }
    };

    const handleAcceptApprove = async () => {
        try {
            if (courseIDAction) {
                const body = {
                    approval: 1,
                };
                await CourseServiceApi.approvalCourseReviews(
                    courseIDAction,
                    body,
                );
                handleOpenApprove(false);
                setCourseReviews((courses: Course[]) => {
                    const updateCourses = [...courses];
                    const courseIndex = updateCourses.findIndex(
                        (course) => course.id === courseIDAction,
                    );

                    if (courseIndex !== -1) {
                        updateCourses.splice(courseIndex, 1);
                        return updateCourses;
                    }

                    return courses;
                });
                toast(<div className="font-bold">Phê duyệt thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">Phê duyệt không thành công!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleAcceptReject = async () => {
        try {
            if (courseIDAction) {
                const body = {
                    approval: 0,
                };

                await CourseServiceApi.approvalCourseReviews(
                    courseIDAction,
                    body,
                );
                handleOpenReject(false);
                setCourseReviews((courses: Course[]) => {
                    const updateCourses = [...courses];
                    const courseIndex = updateCourses.findIndex(
                        (course) => course.id === courseIDAction,
                    );

                    if (courseIndex !== -1) {
                        updateCourses.splice(courseIndex, 1);
                        return updateCourses;
                    }

                    return courses;
                });
                toast(<div className="font-bold">Từ chối thành công!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Từ chối không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    React.useLayoutEffect(() => {
        const fetchCourseReviews = async () => {
            try {
                setLoading(true);
                const coursesResult = await CourseServiceApi.getCourseReviews(
                    10,
                    currentPage,
                );
                // if (infoPagination.totalItem == 0) {

                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: coursesResult.meta.pageCount,
                        totalItem: coursesResult.meta.itemCount,
                        size: coursesResult.meta.size,
                    });
                }
                setCourseReviews(coursesResult.item);

                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };

        fetchCourseReviews();
    }, [currentPage]);

    return (
        <div className="">
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}
                <ApproveCourseModal
                    openModal={openApprove}
                    handleOpenModal={handleOpenApprove}
                    handleAcceptApprove={handleAcceptApprove}
                />

                <RejectCourseModal
                    openModal={openReject}
                    handleOpenModal={handleOpenReject}
                    handleAcceptReject={handleAcceptReject}
                />
                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>Tiêu đề</Table.HeadCell>
                                <Table.HeadCell>Hình ảnh</Table.HeadCell>
                                <Table.HeadCell>Hoạt động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {courseReviews.map((course, index) => (
                                    <Table.Row
                                        key={course.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {course.title}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            <img
                                                className="w-[200px]"
                                                src={
                                                    course.image
                                                        ? course.image
                                                        : ""
                                                }
                                            />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    color="success"
                                                    onClick={() => {
                                                        handleOpenApprove(true);
                                                        setCourseIDAction(
                                                            course.id,
                                                        );
                                                    }}
                                                >
                                                    Chấp thuận
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        handleOpenReject(true);
                                                        setCourseIDAction(
                                                            course.id,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Từ chối
                                                </Button>
                                                <Link
                                                    to={`/courses/${course.id}`}
                                                >
                                                    <Button>Chi tiết</Button>
                                                </Link>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!courseReviews.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
