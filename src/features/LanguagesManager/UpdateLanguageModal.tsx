import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { Language, UpdateLanguage } from '../../models';
import { schemeUpdateLanguage } from '../../validators/category';

export interface IUpdateLanguageModalProps {
    openModal: boolean
    editLanguage: Language | null
    handleModal: (command: boolean) => void
    handleUpdateLanguageData: (data: UpdateLanguage) => void
}

export default function UpdateLanguageModal (props: IUpdateLanguageModalProps) {
    const { openModal, editLanguage, handleModal, handleUpdateLanguageData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<UpdateLanguage>({
        mode: "onChange",
        defaultValues: {
            id: editLanguage ? editLanguage.id : 0,
            name: editLanguage ? editLanguage.name : "",
        },
        resolver: yupResolver(schemeUpdateLanguage),
    });
    
    const handleSubmitUpdateLanguage = (data: UpdateLanguage) => {
        handleUpdateLanguageData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật ngôn ngữ</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdateLanguage)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Update</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
