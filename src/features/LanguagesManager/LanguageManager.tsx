import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import {
    CreateLanguage,
    Language,
    PaginationInfo,
    UpdateLanguage,
} from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreateLanguageModal from "./CreateLanguageModal";
import DeleteLanguageModal from "./DeleteLanguageModal";
import UpdateLanguageModal from "./UpdateLanguageModal";

export interface ILanguageManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};

export default function LanguageManager(props: ILanguageManagerProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [languages, setLanguages] = React.useState<Language[]>([]);
    const [languageEdit, setLanguageEdit] = React.useState<Language | null>(
        null,
    );
    const [languageDelete, setLanguageDelete] = React.useState<Language | null>(
        null,
    );
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setLanguageEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setLanguageDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchLanguages = async () => {
            try {
                const languagesResult = await CategoryServiceApi.getLanguages(
                    10,
                    currentPage,
                );
                if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: languagesResult.meta.pageCount,
                        totalItem: languagesResult.meta.itemCount,
                        size: languagesResult.meta.size,
                    });
                }
                setLanguages(languagesResult.item);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        setLoading(true);

        fetchLanguages();
    }, [currentPage]);

    const handleCreateLanguageData = async (data: CreateLanguage) => {
        try {
            const body = {
                name: data.name,
            };
            const languageData = await CategoryServiceApi.createLanguage(body);
            setLanguages((languages) => [languageData, ...languages]);
            handleOpenCreateModal(false);
            setInfoPagination((pagination) => {
                let totalPage = pagination.totalPage;
                const totalUpdateItem = pagination.totalItem++;

                if (totalUpdateItem % 10 === 0) {
                    totalPage++;
                }
                return {
                    size: pagination.size,
                    totalItem: totalUpdateItem,
                    totalPage,
                };
            });
            toast(<div className="font-bold">Tạo thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateLanguageData = async (data: UpdateLanguage) => {
        try {
            const body = {
                name: data.name,
            };
            const languageData = await CategoryServiceApi.updateLanguage(
                body,
                data.id,
            );

            setLanguages((languages) => {
                const updateLanguages = [...languages];
                const languageIndex = updateLanguages.findIndex(
                    (language) => data.id === language.id,
                );

                if (languageIndex !== -1) {
                    updateLanguages[languageIndex] = languageData;

                    return updateLanguages;
                }

                return languages;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteLanguageData = async () => {
        try {
            if (languageDelete) {
                await CategoryServiceApi.deleteLanguage(languageDelete.id);
                setLanguages((languages) => {
                    const updateLanguages = [...languages];
                    const languageIndex = updateLanguages.findIndex(
                        (language) => languageDelete.id === language.id,
                    );

                    if (languageIndex !== -1) {
                        updateLanguages.splice(languageIndex, 1);
                        return updateLanguages;
                    }

                    return languages;
                });

                handleOpenDeleteModal(false);
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });
                toast(<div className="font-bold">Xóa thành công!!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="">
            <CreateLanguageModal
                openModal={openCreateModal}
                handleModal={handleOpenCreateModal}
                handleCreateLanguageData={handleCreateLanguageData}
            />
            {languageEdit && (
                <UpdateLanguageModal
                    openModal={openUpdateModal}
                    handleModal={handleOpenUpdateModal}
                    handleUpdateLanguageData={handleUpdateLanguageData}
                    editLanguage={languageEdit}
                />
            )}

            {
                <DeleteLanguageModal
                    openModal={openDeleteModal}
                    handleModal={handleOpenDeleteModal}
                    handleDeleteLanguageData={handleDeleteLanguageData}
                />
            }

            <div
                onClick={() => handleOpenCreateModal(true)}
                className="mb-6 flex justify-end"
            >
                {" "}
                <Button className="mr-6" color="success">
                    Create
                </Button>
            </div>
            <div className="overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell className="">
                                    STT
                                </Table.HeadCell>
                                <Table.HeadCell>Value</Table.HeadCell>
                                <Table.HeadCell>Actions</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {languages.map((language, index) => (
                                    <Table.Row
                                        key={language.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {language.name}
                                        </Table.Cell>
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setLanguageEdit(
                                                            language,
                                                        );
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Update
                                                </Button>
                                                <Button
                                                    onClick={() => {
                                                        setLanguageDelete(
                                                            language,
                                                        );
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Delete
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!languages.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {infoPagination.totalPage > 1 && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
