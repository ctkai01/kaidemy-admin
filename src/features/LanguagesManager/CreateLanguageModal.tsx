import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
import { useForm } from 'react-hook-form';
import { CreateLanguage } from '../../models';
import { schemeCreateLanguage } from '../../validators/category';

export interface ICreateLanguageModalProps {
    openModal: boolean
    handleModal: (command: boolean) => void
    handleCreateLanguageData: (data: CreateLanguage) => void
}

export default function CreateLanguageModal (props: ICreateLanguageModalProps) {
    const { openModal, handleModal, handleCreateLanguageData} = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateLanguage>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateLanguage),
    });
    
    const handleSubmitCreateLanguage = (data: CreateLanguage) => {
        handleCreateLanguageData(data)
        reset()
    }

    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Create Language</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreateLanguage)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
