import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, Select, TextInput } from 'flowbite-react';
import * as React from 'react';
import { useForm } from 'react-hook-form';
import { Category, CreatePrice, Price, UpdateCategory, UpdatePrice } from '../../models';
import { schemeCreatePrice, schemeUpdateCategory, schemeUpdatePrice } from '../../validators/category';

export interface IUpdateCategoryModalProps {
    openModal: boolean
    editCategory: Category
    categories: Category[]
    parentCategories: Category[]
    handleModal: (command: boolean) => void
    handleUpdateCategoryData: (data: UpdateCategory) => void
}

export default function UpdateCategoryModal (props: IUpdateCategoryModalProps) {
    const { openModal, editCategory, parentCategories, categories, handleModal, handleUpdateCategoryData, } = props
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        getValues,
        reset,
    } = useForm<UpdateCategory>({
        mode: "onChange",
        defaultValues: {
            id: editCategory ? editCategory.id : 0,
            name: editCategory ? editCategory.name : "",
            parent_id: editCategory && editCategory.parentID  ? editCategory.parentID : undefined,
        },
        resolver: yupResolver(schemeUpdateCategory),
    });
    
    const handleSubmitUpdateCategory = (data: UpdateCategory) => {
        handleUpdateCategoryData(data)
        reset()
    }


    return (
        <Modal dismissible show={openModal} onClose={() => handleModal(false)}>
            <Modal.Header>Cập nhật danh mục</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitUpdateCategory)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                        <div className="mb-2 block">
                            <Label
                                htmlFor="categories"
                                value="Chọn danh mục cha"
                            />
                        </div>
                        <Select
                            id="categories"
                            color={errors.parent_id ? "failure" : ""}
                            {...register("parent_id")}
                            helperText={
                                <>
                                    {errors.parent_id ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>
                                                {errors.parent_id.message}
                                            </span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        >
                            <option value={-1}>-- Chọn danh mục --</option>

                            {parentCategories.map((category) => {
                                if (category.id !== editCategory.id) {
                                    return (
                                        <option
                                            selected={
                                                category.id ===
                                                getValues("parent_id")
                                            }
                                            key={category.id}
                                            value={category.id}
                                        >
                                            {category.name}
                                        </option>
                                    );
                                } else {
                                    return <div></div>;
                                }
                            })}
                        </Select>
                    </div>

                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => handleModal(false)}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Cập nhật</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
