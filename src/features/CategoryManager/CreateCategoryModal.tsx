import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Label, Modal, Select, TextInput } from 'flowbite-react';
import * as React from 'react';
import { useForm } from 'react-hook-form';
import { Category, CreateCategory, CreatePrice } from '../../models';
import { schemeCreateCategory, schemeCreatePrice } from '../../validators/category';

export interface ICreateCategoryModalProps {
    openModal: boolean
    categories: Category[]
    parentCategories: Category[]
    handleModal: (command: boolean) => void
    handleCreateCategoryData: (data: CreateCategory) => void
}

export default function CreateCategoryModal (props: ICreateCategoryModalProps) {
    const { openModal, parentCategories, categories, handleModal, handleCreateCategoryData} = props
  
  
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateCategory>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateCategory),
    });
    
    const handleSubmitCreateCategory = (data: CreateCategory) => {
        handleCreateCategoryData(data)
        reset()
    }

    return (
        <Modal
            dismissible
            show={openModal}
            onClose={() => {
                handleModal(false);
                reset();
            }}
        >
            <Modal.Header>Tạo danh mục</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleSubmitCreateCategory)}>
                    <div>
                        <div className="mb-2 block">
                            <Label htmlFor="name" value="Tên" />
                        </div>
                        <TextInput
                            id="name"
                            type="text"
                            color={errors.name ? "failure" : ""}
                            {...register("name")}
                            helperText={
                                <>
                                    {errors.name ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.name.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                        <div className="mb-2 block">
                            <Label
                                htmlFor="categories"
                                value="Chọn danh mục cha"
                            />
                        </div>
                        <Select
                            id="categories"
                            color={errors.parent_id ? "failure" : ""}
                            {...register("parent_id")}
                            helperText={
                                <>
                                    {errors.parent_id ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>
                                                {errors.parent_id.message}
                                            </span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        >
                            <option value={-1}>-- Chọn danh mục --</option>

                            {parentCategories.map((category) => (
                                <option key={category.id} value={category.id}>
                                    {category.name}
                                </option>
                            ))}
                        </Select>
                    </div>

                    <div className="mt-6 flex justify-end">
                        <Button
                            className="mr-4"
                            onClick={() => {
                                handleModal(false);
                                reset();
                            }}
                            color="gray"
                        >
                            Hủy bỏ
                        </Button>
                        <Button type="submit">Tạo</Button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}
