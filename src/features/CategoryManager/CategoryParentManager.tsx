import {
    Breadcrumb,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    Pagination,
    Select,
    Spinner,
    Table,
} from "flowbite-react";
import * as React from "react";
import { HiHome } from "react-icons/hi";
import { toast } from "react-toastify";
import {
    Category,
    CreateCategory,
    PaginationInfo,
    UpdateCategory,
} from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { formatBetweenNumberPaginate } from "../../utils";
import CreateCategoryModal from "./CreateCategoryModal";
import DeleteCategoryModal from "./DeleteCategoryModal";
import SearchInput from "./SearchInput";
import UpdateCategoryModal from "./UpdateCategoryModal";
// import CreatePriceModal from './CreatePriceModal';
// import DeletePriceModal from './DeletePriceModal';
// import UpdatePriceModal from './UpdatePriceModal';

export interface ICategoryParentManagerProps {}

const customTableTheme: CustomFlowbiteTheme = {
    table: {
        head: {
            cell: {
                base: " bg-gray-100 dark:bg-gray-700 px-6 py-3 border-t border-b",
            },
        },
    },
};
export default function CategoryParentManager(
    props: ICategoryParentManagerProps,
) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [parentCategories, setParentCategories] = React.useState<Category[]>(
        [],
    );
    const [openCreateModal, setOpenCreateModal] = React.useState(false);
    const [openUpdateModal, setOpenUpdateModal] = React.useState(false);
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    const [search, setSearch] = React.useState("");
    const [idFilter, setIdFilter] = React.useState(-1);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [categories, setCategories] = React.useState<Category[]>([]);
    const [categoryEdit, setCategoryEdit] = React.useState<Category | null>(
        null,
    );
    const [categoryDelete, setCategoryDelete] = React.useState<Category | null>(
        null,
    );
    const [loading, setLoading] = React.useState(false);
    const handleOpenCreateModal = (command: boolean) => {
        setOpenCreateModal(command);
    };

    const handleSearch = (data: string) => {
        setSearch(data);
    };

    const handleOpenUpdateModal = (command: boolean) => {
        if (!command) {
            setCategoryEdit(null);
        }
        setOpenUpdateModal(command);
    };

    const handleOpenDeleteModal = (command: boolean) => {
        if (!command) {
            setCategoryDelete(null);
        }
        setOpenDeleteModal(command);
    };

    React.useEffect(() => {
        const fetchCategories = async () => {
            try {
                if (idFilter === -1) {
                    const categoriesResult =
                        await CategoryServiceApi.getCategories(
                            10,
                            currentPage,
                            search,
                        );
                    if (infoPagination.totalItem == 0) {
                        setInfoPagination({
                            totalPage: categoriesResult.meta.pageCount,
                            totalItem: categoriesResult.meta.itemCount,
                            size: categoriesResult.meta.size,
                        });
                    }
                    setCategories(categoriesResult.item);
                } else {
                    const categoriesResult =
                        await CategoryServiceApi.getCategoriesFilter(
                            10,
                            currentPage,
                            idFilter,
                            search,
                        );
                    if (infoPagination.totalItem == 0) {
                        setInfoPagination({
                            totalPage: categoriesResult.meta.pageCount,
                            totalItem: categoriesResult.meta.itemCount,
                            size: categoriesResult.meta.size,
                        });
                    }
                    setCategories(categoriesResult.item);
                }
            } catch (e) {
                console.log(e);
            }
        };

        const fetchParentCategory = async () => {
            try {
                const categoriesResult =
                    await CategoryServiceApi.getParentCategories();
                setParentCategories(categoriesResult.item);
            } catch (e) {
                console.log(e);
            }
        };
        

        const fetchData = async () => {
            await Promise.all([fetchCategories(), fetchParentCategory()]);
            setLoading(false);
        };

        setLoading(true);

        fetchData();
    }, [currentPage, idFilter, search]);

    const handleCreateCategoryData = async (data: CreateCategory) => {
        try {
            const body: any = {
                name: data.name,
            };
            if (data.parent_id !== -1) {
                body.parentID = data.parent_id;
            }
            const categoryData = await CategoryServiceApi.createCategory(body);
            setCategories((categories) => {
                if (categories.length < infoPagination.size) {
                    return [categoryData, ...categories];
                } else {
                    return categories;
                }
            });
            handleOpenCreateModal(false);
            if (data.parent_id == -1) {
                setParentCategories((pre) => [...pre, categoryData]);
            }
                // const categoriesResult = await CategoryServiceApi.getCategories(
                //     10,
                //     1,
                // );
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem++;

                    if (totalUpdateItem % 10 === 0) {
                        totalPage++;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });

            toast(<div className="font-bold">Tạo thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Tạo không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateCategoryData = async (data: UpdateCategory) => {
        try {
            const body: any = {
                name: data.name,
            };
            if (data.parent_id !== -1) {
                body.parentID = data.parent_id;
            }

            // if (data.parent_id !== -1) {
            //   formData.append('parent_id', `${data.parent_id}`)
            // } else {
            //   formData.append('parent_id', `0`)
            // }
            const priceData = await CategoryServiceApi.updateCategory(
                body,
                data.id,
            );

            setCategories((categories) => {
                const updateCategories = [...categories];
                const categoryIndex = updateCategories.findIndex(
                    (price) => data.id === price.id,
                );

                if (categoryIndex !== -1) {
                    updateCategories[categoryIndex] = priceData;

                    return updateCategories;
                }

                return categories;
            });
            handleOpenUpdateModal(false);
            toast(<div className="font-bold">Cập nhật thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Cập nhật không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteCategoryData = async () => {
        try {
            if (categoryDelete) {
                await CategoryServiceApi.deleteCategory(categoryDelete.id);
                setCategories((categories) => {
                    const updateCategories = [...categories];
                    const categoryIndex = updateCategories.findIndex(
                        (category) => categoryDelete.id === category.id,
                    );

                    if (categoryIndex !== -1) {
                        updateCategories.splice(categoryIndex, 1);
                        return updateCategories;
                    }

                    return categories;
                });

                handleOpenDeleteModal(false);
                setInfoPagination((pagination) => {
                    let totalPage = pagination.totalPage;
                    const totalUpdateItem = pagination.totalItem--;

                    if ((totalUpdateItem + 1) % 10 === 0) {
                        totalPage--;
                    }
                    return {
                        size: pagination.size,
                        totalItem: totalUpdateItem,
                        totalPage,
                    };
                });
                toast(<div className="font-bold">Xóa thành công!!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                });
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Xóa không thành công!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleChangeFilter = async (
        e: React.ChangeEvent<HTMLSelectElement>,
    ) => {
        const value = e.target.value;
        setIdFilter(+value);
    };

    return (
        <div className="">
            <CreateCategoryModal
                openModal={openCreateModal}
                parentCategories={parentCategories}
                handleModal={handleOpenCreateModal}
                categories={categories}
                handleCreateCategoryData={handleCreateCategoryData}
            />

            {categoryEdit && (
                <UpdateCategoryModal
                    categories={categories}
                    openModal={openUpdateModal}
                    parentCategories={parentCategories}
                    handleModal={handleOpenUpdateModal}
                    handleUpdateCategoryData={handleUpdateCategoryData}
                    editCategory={categoryEdit}
                />
            )}

            <DeleteCategoryModal
                openModal={openDeleteModal}
                handleModal={handleOpenDeleteModal}
                handleDeleteCategoryData={handleDeleteCategoryData}
            />

            {/* <div className='ml-4'>
      <Breadcrumb aria-label="Default breadcrumb example">
            <Breadcrumb.Item href="#" icon={HiHome}>
              Home
            </Breadcrumb.Item>
            <Breadcrumb.Item>Categories</Breadcrumb.Item>
          </Breadcrumb>

            </div>
        */}
            <div className="ml-4 flex">
                <div>
                    <div className="mb-2 block">
                        <Label value="Lọc danh mục con" />
                    </div>
                    <div className="flex gap-6">
                        <Select onChange={handleChangeFilter}>
                            <option value={-1}>-- Chọn danh mục --</option>

                            {parentCategories.map((category) => (
                                <option key={category.id} value={category.id}>
                                    {category.name}
                                </option>
                            ))}
                        </Select>
                        <SearchInput
                            handleSearch={handleSearch}
                            search={search}
                        />
                    </div>
                </div>
            </div>

            <div className="mb-6 flex justify-end">
                {" "}
                <Button
                    onClick={() => handleOpenCreateModal(true)}
                    className="mr-6"
                    color="success"
                >
                    Tạo
                </Button>
            </div>
            <div className="mb-6 overflow-x-auto border-b ">
                {loading && (
                    <div className="text-center">
                        <Spinner
                            aria-label="Extra large spinner example"
                            size="xl"
                        />
                    </div>
                )}

                {!loading && (
                    <Flowbite theme={{ theme: customTableTheme }}>
                        <Table hoverable>
                            <Table.Head>
                                <Table.HeadCell>STT</Table.HeadCell>

                                <Table.HeadCell>Tên</Table.HeadCell>
                                <Table.HeadCell>Hành động</Table.HeadCell>
                            </Table.Head>
                            <Table.Body className="divide-y">
                                {categories.map((category, index) => (
                                    <Table.Row
                                        key={category.id}
                                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                                    >
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {10 * (currentPage - 1) + index + 1}
                                        </Table.Cell>
                                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                                            {category.name}
                                        </Table.Cell>
                                        {/* <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">${price.value}</Table.Cell> */}
                                        <Table.Cell>
                                            <div className="flex gap-2">
                                                <Button
                                                    onClick={() => {
                                                        setCategoryEdit(
                                                            category,
                                                        );
                                                        handleOpenUpdateModal(
                                                            true,
                                                        );
                                                    }}
                                                >
                                                    Cập nhật
                                                </Button>
                                                {/* <Button onClick={() => {
                    // setCategoryEdit(category)
                    // handleOpenUpdateModal(true)
                  }}
                  color="warning"
                  >
                    SubCategory
                  </Button> */}
                                                <Button
                                                    onClick={() => {
                                                        setCategoryDelete(
                                                            category,
                                                        );
                                                        handleOpenDeleteModal(
                                                            true,
                                                        );
                                                    }}
                                                    color="failure"
                                                >
                                                    Xóa bỏ
                                                </Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Flowbite>
                )}
                {!categories.length && !loading && (
                    <div className="py-4 text-center font-bold">
                        Danh sách trống
                    </div>
                )}
                {(infoPagination.totalPage > 1 ||
                    categories.length > infoPagination.size) && (
                    <>
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                        <div className="mt-4 text-center text-xs text-primary-gray">
                            {formatBetweenNumberPaginate(
                                currentPage,
                                infoPagination.size,
                                infoPagination.totalItem,
                            )}{" "}
                            of {infoPagination.totalItem} items
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
