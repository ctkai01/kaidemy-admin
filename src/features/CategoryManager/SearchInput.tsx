import { TextInput } from 'flowbite-react';
import { IoMdSearch } from "react-icons/io";
export interface ISearchInputProps {
  search: string,
  handleSearch: (data: string) => void
}

export default function SearchInput (props: ISearchInputProps) {
  const { search, handleSearch }  = props
  return (
      <div>
          <div className="max-w-md">
              <TextInput
                  onChange={(e) => handleSearch(e.target.value)}
                  value={search}
                  type="text"
                  icon={IoMdSearch}
                  placeholder=""
                  required
              />
          </div>
      </div>
  );
}
