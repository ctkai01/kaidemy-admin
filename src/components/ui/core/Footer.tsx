import { Footer as FooterFlowBite } from "flowbite-react";
import kaidemyIconBlack from "../../../assets/svg/kaidemy-icon-black.svg";

const Footer = () => {
    const currentYear = new Date().getFullYear();
    return (
        <FooterFlowBite
            container
            className="mt-20  rounded-none bg-primary-black text-white"
        >
            <div className="w-full text-center">
                <div className="w-full justify-between sm:flex sm:items-center sm:justify-between">
                    <FooterFlowBite.Brand
                        href="https://flowbite.com"
                        src={kaidemyIconBlack}
                        alt="Flowbite Logo"
                        // name="Flowbite"
                    />
                   
                </div>
                <FooterFlowBite.Divider className="h-[2px] text-white" />
                <FooterFlowBite.Copyright
                    href="#"
                    by="Kaidemy"
                    year={currentYear}
                    className="font-bold text-white"
                />
            </div>
        </FooterFlowBite>
    );
};

export default Footer;
