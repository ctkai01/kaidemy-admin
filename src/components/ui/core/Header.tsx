import { Avatar, Dropdown, Navbar } from "flowbite-react";
import * as React from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../hook/redux-hook";
import {
    logout,
    selectUserAuth,
} from "../../../services/state/redux/authSlide";
import { akaName, clearAuthLocalStorage } from "../../../utils";

export interface IHeaderProps {}

export default function Header(props: IHeaderProps) {
    const navigate = useNavigate();
    const user = useAppSelector(selectUserAuth);

    const dispatch = useAppDispatch();

    const handleLogout = () => {
        dispatch(logout());
        clearAuthLocalStorage();
        navigate("/login");
    };

    return (
        <div className="sticky left-0 top-0 z-50 border-b border-primary-border">
            {user && (
                <Navbar fluid rounded>
                    <Navbar.Brand href="/" className="ml-[80px]">
                        <img
                            src="https://kaidemy.b-cdn.net/avatar/hustdemy-high-logo-transparent-black.png"
                            className="mr-3 h-6 sm:h-9"
                            alt="Flowbite React Logo"
                        />
                    </Navbar.Brand>
                    <div className="flex md:order-2">
                        <Dropdown
                            arrowIcon={false}
                            inline
                            label={
                                // <Avatar alt="User settings" img="https://flowbite.com/docs/images/people/profile-picture-5.jpg" rounded />
                                user?.avatar ? (
                                    <Avatar
                                        img={user.avatar}
                                        alt="avatar"
                                        rounded
                                        size="sm"
                                    />
                                ) : (
                                    <Avatar
                                        placeholderInitials={akaName(
                                            user ? user.name : "",
                                        )}
                                        rounded
                                        size="sm"
                                    />
                                )
                            }
                        >
                            <Dropdown.Header>
                                <span className="block text-sm">
                                    {user?.name}
                                </span>
                                <span className="block truncate text-sm font-medium">
                                    {user?.email}
                                </span>
                            </Dropdown.Header>
                            {/* <Dropdown.Item>Dashboard</Dropdown.Item> */}
                            {/* <Dropdown.Item>Settings</Dropdown.Item> */}
                            {/* <Dropdown.Item>Earnings</Dropdown.Item> */}
                            <Dropdown.Divider />
                            <Dropdown.Item onClick={handleLogout}>
                                Đăng xuất
                            </Dropdown.Item>
                        </Dropdown>
                        <Navbar.Toggle />
                    </div>
                    {/* <Navbar.Collapse>
        <Navbar.Link href="#" active>
          Home
        </Navbar.Link>
        <Navbar.Link href="#">About</Navbar.Link>
        <Navbar.Link href="#">Services</Navbar.Link>
        <Navbar.Link href="#">Pricing</Navbar.Link>
        <Navbar.Link href="#">Contact</Navbar.Link>
      </Navbar.Collapse> */}
                </Navbar>
            )}
        </div>
    );
}
