import { Sidebar as SidebarFlow } from "flowbite-react";
import { AiFillThunderbolt } from "react-icons/ai";
import { BiSolidCategory } from "react-icons/bi";
import { FaChartBar, FaQuestion } from "react-icons/fa";
import { GrUserAdmin } from "react-icons/gr";
import { HiShoppingBag, HiUser } from "react-icons/hi";
import { HiLanguage } from "react-icons/hi2";
import { MdApproval } from "react-icons/md";
import { RiFolderVideoFill } from "react-icons/ri";
import { TbReport } from "react-icons/tb";
import { Link, useLocation } from "react-router-dom";
import { useAppSelector } from "../../../hook/redux-hook";
import { selectUserAuth } from "../../../services/state/redux/authSlide";
import { ADMIN_SUPPER_ROLE } from "../../../constants";
export interface ISidebarProps {}

export default function Sidebar(props: ISidebarProps) {
    const location = useLocation();
    const user = useAppSelector(selectUserAuth);
    const isSupperAdmin = user?.role === ADMIN_SUPPER_ROLE;

    return (
        <SidebarFlow
            className="h-61-calc sticky left-0 top-[61px] border-r border-primary-border"
            aria-label="Sidebar with multi-level dropdown example"
        >
            <SidebarFlow.Items>
                <SidebarFlow.ItemGroup>
                    {/* <SidebarFlow.Item active={location.pathname.startsWith('/levels')}  href="/levels" icon={AiFillThunderbolt}>
            Levels
          </SidebarFlow.Item>
          <SidebarFlow.Item active={location.pathname.startsWith('/languages')}  href="/languages" icon={HiLanguage}>
            Languages
          </SidebarFlow.Item> */}
                    {/* <SidebarFlow.Item active={location.pathname.startsWith('/issue-types')}  href="/issue-types" icon={FaQuestion}>
            Issue Types
          </SidebarFlow.Item> */}
                    {/* <SidebarFlow.Item  active={location.pathname.startsWith('/prices')}   icon={HiShoppingBag}> */}

                    {/* GrUserAdmin */}
                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname === "/"
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/"} className="flex p-2">
                            <FaChartBar className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>
                                Bảng điều khiển
                            </span>
                        </Link>
                    </li>
                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/courses")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/courses"} className="flex p-2">
                            <RiFolderVideoFill className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Khóa học</span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/users")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/users"} className="flex p-2">
                            <HiUser className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>
                                Người dùng
                            </span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/approve-courses")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/approve-courses"} className="flex p-2">
                            <MdApproval className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>
                                Phê duyệt khóa học
                            </span>
                        </Link>
                    </li>
                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/reports")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/reports"} className="flex p-2">
                            <TbReport className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Báo cáo</span>
                        </Link>
                    </li>
                    {isSupperAdmin && (
                        <li
                            className={` hover:bg-gray-100 ${
                                location.pathname.startsWith("/admins")
                                    ? "text-cyan-600"
                                    : "text-gray-500"
                            }`}
                        >
                            <Link to={"/admins"} className="flex p-2">
                                <GrUserAdmin className="h-6 w-6" />
                                <span className={`px-3 font-bold  `}>
                                    Quản trị viên
                                </span>
                            </Link>
                        </li>
                    )}

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/levels")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/levels"} className="flex p-2">
                            <AiFillThunderbolt className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Cấp độ</span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/languages")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/languages"} className="flex p-2">
                            <HiLanguage className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Ngôn ngữ</span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/categories")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/categories"} className="flex p-2">
                            <BiSolidCategory className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Danh mục</span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/issue-types")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/issue-types"} className="flex p-2">
                            <FaQuestion className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>
                                Loại vấn đề
                            </span>
                        </Link>
                    </li>

                    <li
                        className={` hover:bg-gray-100 ${
                            location.pathname.startsWith("/prices")
                                ? "text-cyan-600"
                                : "text-gray-500"
                        }`}
                    >
                        <Link to={"/prices"} className="flex p-2">
                            <HiShoppingBag className="h-6 w-6" />
                            <span className={`px-3 font-bold  `}>Giá</span>
                        </Link>
                    </li>

                    {/* </SidebarFlow.Item> */}
                    {/* <SidebarFlow.Item href="#" icon={HiArrowSmRight}>
            Sign In
          </SidebarFlow.Item>
          <SidebarFlow.Item href="#" icon={HiTable}>
            Sign Up
          </SidebarFlow.Item> */}
                </SidebarFlow.ItemGroup>
            </SidebarFlow.Items>
        </SidebarFlow>
    );
}
